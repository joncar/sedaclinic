<script src='https://www.google.com/recaptcha/api.js?render=6Lfxy4wUAAAAAE1O6q6N6DHINBUx90cwAyqjb9wG'></script>
<!--Scroll to top-->
<script src="<?= base_url() ?>theme/theme/js/jquery.js"></script> 
<script src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>
<!--Revolution Slider-->
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url() ?>theme/theme/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/main-slider-script.js"></script>
<!--End Revolution Slider-->
<script src="<?= base_url() ?>theme/theme/js/jquery.fancybox.js"></script>
<script src="<?= base_url() ?>theme/theme/js/jquery.timepicker.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/jquery-ui.js"></script>
<script src="<?= base_url() ?>theme/theme/js/owl.js"></script>
<script src="<?= base_url() ?>theme/theme/js/appear.js"></script>
<script src="<?= base_url() ?>theme/theme/js/mixitup.js"></script>
<script src="<?= base_url() ?>theme/theme/js/wow.js"></script>
<script src="<?= base_url() ?>theme/theme/js/validate.js"></script>
<script src="<?= base_url() ?>theme/theme/js/script.js"></script>
<!--Google Map APi Key-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="<?= base_url() ?>theme/theme/js/map-script.js"></script>
<!--End Google Map APi-->


<script src="//cdn.jsdelivr.net/npm/jquery.marquee@1.5.0/jquery.marquee.min.js" type="text/javascript"></script>
<!-- Social-feed js -->
<script src="<?= base_url() ?>js/social/bower_components/codebird-js/codebird.js"></script>
<script src="<?= base_url() ?>js/social/bower_components/doT/doT.min.js"></script>
<script src="<?= base_url() ?>js/social/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url() ?>js/social/bower_components/moment/locale/it.js"></script>
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js"></script>
<script>
    $(document).ready(function(){    	
    	$('.marquee').socialfeed({            
            // INSTAGRAM
            twitter:{
                accounts: ['@sedaclinic'],  //Array: Specify a list of accounts from which to pull posts
                limit: 2,                         
                consumer_key: 'JB0T9F9gDxzMxxIamSIEA0fHA',       //String: Instagram client id (option if using access token)
                consumer_secret: 'MWDrS2GmSRUVkHWIAqCkAlVK3Z21Pvc7stCAPbVzRfoHzr7pUy' //String: Instagram access token
            },                          //Integer: Number of seconds before social-feed will attempt to load new posts.            
            template: "<?= base_url() ?>js/social/js/template2.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en",
            callback:function(){   
            	$('.marquee').marquee({
                    //duration in milliseconds of the marquee
                    duration: 15000,
                    //gap in pixels between the tickers
                    gap: 50,
                    //time in milliseconds before the marquee will start animating
                    delayBeforeStart: 0,
                    //'left' or 'right'
                    direction: 'left',
                    //true or false - should the marquee be duplicated to show an effect of continues flow
                    duplicated: true
                });
            }
        });

        $('.social-feed-container').socialfeed({            
            // INSTAGRAM
            instagram:{
                accounts: ['@sedaclinicl'],  //Array: Specify a list of accounts from which to pull posts
                limit: 6,         
                type:'instagram',                         //Integer: max number of posts to load
                client_id: 'a02e390bce1c4f49ac1aa38b44a3137e',       //String: Instagram client id (option if using access token)
                access_token: '9777375966.a02e390.a2f63e12d24344c896cbd3ad0904bc9c' //String: Instagram access token
            },                          //Integer: Number of seconds before social-feed will attempt to load new posts.
            template: "<?= base_url() ?>js/social/js/template.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en"
        });

        
    });
</script>
<script src="<?= base_url() ?>js/frame.js"></script>