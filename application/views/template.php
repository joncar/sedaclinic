<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

	<script>var URL = '<?= base_url() ?>';</script>

	<!-- Stylesheets -->
	<link href="<?= base_url() ?>theme/theme/css/bootstrap.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
	<link href="<?= base_url() ?>theme/theme/plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
	<link href="<?= base_url() ?>theme/theme/plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
	<link href="<?= base_url() ?>theme/theme/css/flaticon/flaticon.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/css/style_custom.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/css/responsive.css" rel="stylesheet">

	<!--Color Themes-->
	<link id="theme-color-file" href="<?= base_url() ?>theme/theme/css/color-themes/default-theme.css" rel="stylesheet">
	<!-- Responsive -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
	<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
	
</head>

<body>	
	<div class="page-wrapper">
	    <!-- Preloader -->
	    <div class="preloader"></div>

		<?php 
			if(empty($editor)){
				$this->load->view($view); 
			}else{
				echo $view;
			}
		?>	
	</div>
	
	<?php $this->load->view('includes/template/scripts') ?>
</body>
</html>