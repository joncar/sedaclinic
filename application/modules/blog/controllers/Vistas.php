<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Vistas extends Main{
        function __construct() {
            parent::__construct();       
            $this->load->model('querys');     
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('blog_categorias');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('blog',array('blog_categorias_id'=>$c->id))->num_rows();
                $categorias->row($n)->link = base_url('blog').'?categorias_id='.$c->id;
            }
            return $categorias;
        }
        
        
        public function portafolio($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $blog = new Bdsource();
                $blog->where('id',$id);
                $blog->init('blog',TRUE);
                $this->blog->link = site_url('blog/'.toURL($this->blog->id.'-'.$this->blog->titulo));
                $this->blog->foto = base_url('img/blog/'.$this->blog->foto);
                $this->blog->foto2 = base_url('img/blog/'.$this->blog->foto2);
                $this->blog->foto3 = base_url('img/blog/'.$this->blog->foto3);
                $this->blog->mes = strftime('%b',strtotime($this->blog->fecha));
                $this->blog->dia = strftime('%d',strtotime($this->blog->fecha));
                $this->blog->fecha = strftime('%d %b %Y',strtotime($this->blog->fecha));
                if($blog->num_rows()>0){
                    $blog->vistas++;
                    $blog->save();
                }
                $comentarios = new Bdsource();
                $comentarios->where('blog_id',$this->blog->id);
                $comentarios->init('comentarios');
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('blog_categorias_id',$this->blog->blog_categorias_id);
                $relacionados->where('id !=',$this->blog->id);
                $relacionados->where('idioma',$_SESSION['lang']);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('blog',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                    $this->relacionados->row($n)->foto = base_url('img/blog/'.$b->foto);
                    $this->relacionados->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
                }

                $prev = '';
                $next = '';
                $blogs = $this->db->get_where('blog',array('status'=>1,'idioma'=>$_SESSION['lang'],'blog_categorias_id'=>3));
                $entradas = array();
                foreach($blogs->result() as $n=>$b){
                    $entradas[] = $b;
                }
                foreach($entradas as $n=>$b){
                    if(!empty($entradas[$n-1])){
                        $bb = $entradas[$n-1];                        
                        $prev = '<a href="'.site_url('blog/'.toUrl($bb->id.'-'.$bb->titulo)).'" class="prev"><span class="fa fa-long-arrow-left"></span> Prev</a>';
                    }

                    if(!empty($entradas[$n+1])){
                        $bb = $entradas[$n+1];                        
                        $next = '<a href="'.site_url('blog/'.toUrl($bb->id.'-'.$bb->titulo)).'" class="next">Sig <span class="fa fa-long-arrow-right"></span></a>';
                    }
                }
                $page = $this->load->view($this->theme.'portafolio_detail',array('detail'=>$this->blog),TRUE,'paginas');
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->blog,
                        'title'=>$this->blog->titulo,
                        'comentarios'=>$this->comentarios,
                        'categorias'=>$this->get_categorias(),
                        'relacionados'=>$this->relacionados,
                        'prev'=>$prev,
                        'next'=>$next,
                        'page'=>$page,
                        'link'=>'blog'
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        
    }
?>
