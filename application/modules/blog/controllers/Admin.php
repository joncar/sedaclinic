<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            $crud->where('blog_categorias_id',2)
                 ->field_type('blog_categorias_id','hidden',2)
                 ->field_type('blog_subcategorias_id','hidden',9);
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'770px','height'=>'500px'));
            $crud->set_relation_dependency('blog_subcategorias_id','blog_categorias_id','blog_categorias_id');
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->columns("foto","titulo","tags","fecha","idioma",'orden');
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('blog/admin/clonarEntrada').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function servicios(){
            $this->as['servicios'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->where('blog_categorias_id',1)
                 ->field_type('blog_categorias_id','hidden',1)
                 ->field_type('blog_subcategorias_id','hidden',10);
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'770px','height'=>'500px'));
            $crud->field_type('foto2','image',array('path'=>'img/blog','width'=>'770px','height'=>'500px'));
            $crud->set_field_upload('icono','img/blog');
            /*$crud->set_field_upload('foto','img/blog');
            $crud->set_field_upload('foto2','img/blog');
            $crud->set_field_upload('foto3','img/blog');*/
            $crud->set_relation_dependency('blog_subcategorias_id','blog_categorias_id','blog_categorias_id');
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->columns("blog_categorias_id","foto","titulo","tags","fecha","idioma",'orden');            
            $crud->add_action('Iconos','',base_url('blog/admin/servicios_iconos').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function servicios_iconos($x = ''){            
            $crud = $this->crud_function('','');
            $crud->where('blog_id',$x)
                 ->field_type('blog_id','hidden',$x)
                 ->set_field_upload('icono','img/blog');
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function equipo(){
            $this->as['equipo'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->where('blog_categorias_id',3)
                 ->field_type('blog_categorias_id','hidden',3)
                 ->field_type('blog_subcategorias_id','hidden',11);
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'490px','height'=>'580px'));
            /*$crud->set_field_upload('foto','img/blog');
            $crud->set_field_upload('foto2','img/blog');
            $crud->set_field_upload('foto3','img/blog');*/
            $crud->set_relation_dependency('blog_subcategorias_id','blog_categorias_id','blog_categorias_id');
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->columns("foto","titulo","tags","fecha","idioma",'orden');
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('blog/admin/clonarEntrada').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function blog_categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Sub Categoria');
            $crud->display_as('blog_categorias_nombre','Nombre');
            $crud->field_type('banner','image',array('path'=>'img/blog','width'=>'1700px','height'=>'500px'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }

        public function blog_subcategorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('SubCategoria');            
            $crud->field_type('banner','image',array('path'=>'img/blog','width'=>'1700px','height'=>'500px'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud = $crud->render();
            $crud->title = 'SubCategoria';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }

        public function cursos(){
            $this->as['cursos'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->where('blog_categorias_id',4)
                 ->field_type('blog_categorias_id','hidden',4)
                 ->field_type('blog_subcategorias_id','hidden',12);
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'770px','height'=>'500px'));
            $crud->field_type('foto2','image',array('path'=>'img/blog','width'=>'770px','height'=>'500px'));
            $crud->set_field_upload('icono','img/blog');
            /*$crud->set_field_upload('foto','img/blog');
            $crud->set_field_upload('foto2','img/blog');
            $crud->set_field_upload('foto3','img/blog');*/
            $crud->set_relation_dependency('blog_subcategorias_id','blog_categorias_id','blog_categorias_id');
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->columns("blog_categorias_id","foto","titulo","tags","fecha","idioma",'orden');            
            $crud->add_action('Iconos','',base_url('blog/admin/servicios_iconos').'/');
            $crud->field_type('user','string',$this->user->nombre);
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function testimonios(){
            $this->as['testimonios'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->where('blog_categorias_id',6)
                 ->field_type('blog_categorias_id','hidden',6)
                 ->field_type('blog_subcategorias_id','hidden',13);
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'180px','height'=>'180px'))
                 ->field_type('foto2','image',array('path'=>'img/blog','width'=>'195px','height'=>'60px'))
                 ->display_as('foto2','Logo');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->fields('blog_categorias_id','blog_subcategorias_id','foto','foto2','titulo','subtitulo','texto','status','idioma')
                 ->columns('foto','foto2','titulo','subtitulo','texto','status','idioma');

            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function socios(){            
            $crud = $this->crud_function('','');
            $crud->set_order('orden');
            $crud->field_type('logo','image',array('path'=>'theme/theme/images/clients/','width'=>'240px','height'=>'120px'));
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
