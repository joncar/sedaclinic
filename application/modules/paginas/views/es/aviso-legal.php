[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_6.jpg);">
    <div class="auto-container">
        <h1>Aviso legal</h1>
        <ul class="bread-crumb clearfix">
            <li><a href="[base_url]">Inicio </a></li>    
            <li>Aviso legal</li>        
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Doctors Section -->
    <section class="department-section" style="padding-bottom: 0">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Aviso legal</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div>
            <div>
				<h4>Quién es el responsable del tratamiento de sus datos personales?</h4> <br>
<p>REANIMA SLP es el responsable del tratamiento de los datos personales que usted nos proporciona y se responsabiliza de los referidos datos personales de acuerdo con la normativa aplicable en materia de protección de datos. 
<br>REANIMA SLP con domicilio social en calle Avda Industria 53 08960 Sant Just Desvern; con CIF número B-66312695 e inscrita en el Registro Mercantil de Barcelona.
<br>E-mail de contacto: info@sedaclinic.com
</p>
<br><br><h4>Donde almacenamos sus datos?</h4> <br>
<p>Los datos que recopilamos sobre usted se almacenan dentro del Espacio Económico Europeo («EEE»). Cualquier transferencia de sus datos personales será realizada en conformidad con las leyes aplicables.
</p>
<br><br><h4>¿A quien comunicamos sus datos?</h4> <br>
<p>Sus datos pueden ser compartidos por la empresa REANIMA SLP. Nunca pasamos, vendemos ni intercambiamos sus datos personales con terceras partes. Los datos que se envíen a terceros, se utilizarán únicamente para ofrecerle a usted nuestros servicios. Le detallaremos las categorías de terceros que acceden a sus datos para cada actividad de tratamiento específico.
</p>
<br><br><h4>¿Qué es la base legal para el tratamiento de sus datos personales? </h4> <br>
<p>En cada tratamiento específico de datos personales recopilados sobre usted, le informaremos si la comunicación de datos personales es un requisito legal o contractual, o un requisito necesario para subscribir un contrato, y si está obligado a facilitar los datos personales, así como de las posibles consecuencias de no facilitar tales datos.
</p>
<br><br><h4>¿Qué son sus derechos? </h4> <br>
<h5>Derecho de acceso: </h5> <br>
<p>Cualquier persona tiene derecho a obtener confirmación sobre si en REANIMA SLP estamos tratando datos personales que los conciernan, o no. Puede contactar a info@sedaclinic.com que le remitirá los datos personales que tratamos sobre usted por email.
</p>
<br><h5>Derecho de portabilidad: </h5> <br>
<p>Siempre que REANIMA SLP procese sus datos personales a través de medios automatizados en base a su consentimiento o a un acuerdo, usted tiene derecho a obtener una copia de sus datos en un formato estructurado, de uso común y lectura mecánica transferida a su nombre o a un tercero. En ella se incluirán únicamente los datos personales que usted nos haya facilitado.
</p>
<br><h5>Derecho de rectificación:&nbsp;&nbsp;</h5> <br>
<p>Usted tiene derecho a solicitar la rectificación de sus datos personales si son inexactos, incluyendo el derecho a completar datos que figuren incompletos.
</p>
<br><h5>Derecho de supresión:&nbsp;&nbsp;</h5> <br>
<p>Usted tiene derecho a obtener sin más dilación indebida la supresión de cualquier dato personal suyo tratada por REANIMA SLP a cualquier momento, excepto en las siguientes situaciones: <br> tiene una deuda pendiente con REANIMA SLP, independientemente del método de pago
<br>* se sospecha o está confirmado que ha utilizado incorrectamente nuestros servicios en los últimos cuatro años
<br>* ha contratado algún servicio por el que conservaremos sus datos personales en relación con la transacción por normativa contable.

</p>
<br><h5>Derecho de oposición al tratamiento de datos en base al interés legítimo: </h5> <br>
<p>Usted tiene derecho a oponerse al tratamiento de sus datos personales en base al interés legítimo de REANIMA SLP. REANIMA SLP no seguirá tratando los datos personales salvo que podamos acreditar motivos legítimos imperiosos para el tratamiento que prevalezcan sobre sus intereses, derechos y libertades, o bien para la formulación, el ejercicio o la defensa de reclamaciones.
</p>
<p></p>
<br><h5>Derecho de oposición al marketing directo: </h5> <br>
<p>Usted tiene derecho a oponerse al marketing directo, incluyendo la elaboración de perfiles realizada para este marketing directo.<br>
Puede desvincularse del marketing directo en cualquier momento de las siguientes maneras: siguiendo las indicaciones facilitadas a cada correo de marketing

</p>
<p></p>
<br><h5>Derecho a presentar una reclamación ante una autoridad de control: </h5> <br>
<p>Si usted considera que REANIMA SLP trata sus datos de una manera incorrecta, puede contactar con nosotros. También tiene derecho a presentar una queja ante la autoridad de protección de datos competente.
</p>
<br><h5>Derecho a limitación en el tratamiento: </h5> <br>
<p>Usted tiene derecho a solicitar que REANIMA SLP limite el tratamiento de sus datos personales en las siguientes circunstancias:
<br>* si usted se opone al tratamiento de sus datos en base al interés legítimo de REANIMA SLP. REANIMA SLP tendrá que limitar cualquier tratamiento de estos datos a la espera de la verificación del interés legítimo.
<br>* si usted reclama que sus datos personales son incorrectos, REANIMA SLP tiene que limitar cualquier tratamiento de estos datos hasta que se verifique la precisión de los mismos.
<br>* si el tratamiento es ilegal, usted podrá oponerse al hecho que se eliminen los datos personales y, en su lugar, solicitar la limitación de su uso.
<br>* si REANIMA SLP ya no necesita los datos personales, pero usted los necesita para la formulación, el ejercicio o la defensa de reclamaciones.

</p><br><br>
<h4>Ejercicio de derechos: </h4> <br>
<p>Nos tomamos muy de verdad la protección de datos y, por lo tanto, contamos con personal de servicio al cliente dedicado que se ocupa de sus solicitudes en relación con los derechos antes mencionados. Siempre puede posarse en contacto con ellos en: info@sedaclinic.com
				</p>
						</div>
        </div>
    </section>
    <!-- End Doctors Section -->

    
    <!-- Call To Action -->
    <section class="call-to-action" style="margin-top:50px; background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>¿Quieres trabajar con nosotros?</h2>
                    <p>Rellena el siguiente formulario y enseguida nos pondremos en contacto, ¡estaremos encantados! 
                    </p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacto</a>
                </div>
            </div>
        </div>
    </section>

[footer]