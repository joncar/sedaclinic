[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_6.jpg);">
    <div class="auto-container">
        <h1>Resultados de busqueda</h1>
        <ul class="bread-crumb clearfix">
            <li><a href="[base_url]">Inici </a></li>    
            <li>Resultados de busqueda</li>        
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Doctors Section -->
<section class="department-section" style="padding-bottom: 0">
    <div class="auto-container">
        <?php foreach($resultado as $r): ?>
			<?= $r ?>
		<?php endforeach ?>
    </div>
</section>
<!-- End Doctors Section -->

 
[footer]