[menu]

<!--Main Slider-->
    <section class="main-slider">
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>
                    <!-- Slide 1 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1684" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/sl2.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Bienvenidos <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Profesionales de la sedación experta</h2>
                        </div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servicio/odontologia" class="theme-btn btn-style-one">Servicios</a>
                        </div>
                    </li> 
                    <!-- Slide 1 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1685" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/1.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Bienvenidos <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Te aportamos tranquilidad</h2>
                        </div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servicio/odontologia" class="theme-btn btn-style-one">Servicios</a>
                        </div>
                    </li> 

                    <!-- Slide 2 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1686" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/image-2.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Bienvenidos <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Garantizamos una experiencia positiva </h2>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['490','490','490','490']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <!-- 
<p>Qualified Staff With Expertise in Services We Offer for more Resonable cost with love, Just explore about More!</p>
                        
 --></div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servicio/odontologia" class="theme-btn btn-style-one">Servicios</a>
                        </div>

                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="shape"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['-85','-85','-550','-672']"
                        data-voffset="['0','0','0','0']"
                        data-x="['right','right','right','right']"
                        data-y="['bottom','bottom','bottom','bottom']"
                        data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <figure class="content-image"><img src="[base_url]theme/theme/images/main-slider/content-img-1.png" alt=""></figure>
                        </div>
                    </li>                  
                
                	 <!-- Slide 3 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-3.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/image-3.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Bienvenidos <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Acompañamos al paciente</h2>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['490','490','490','490']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <!-- 
<p>Qualified Staff With Expertise in Services We Offer for more Resonable cost with love, Just explore about More!</p>
                        
 --></div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servicio/odontologia" class="theme-btn btn-style-one">Servicios</a>
                        </div>

                        <!--<div class="tp-caption tp-resizeme big-ipad-hidden" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="shape"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['-85','-85','-65','-65']"
                        data-voffset="['0','0','0','0']"
                        data-x="['right','right','right','right']"
                        data-y="['bottom','bottom','bottom','bottom']"
                        data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <figure class="content-image"><img src="[base_url]theme/theme/images/main-slider/content-img-2.png" alt=""></figure>
                        </div>-->
                    </li>                  
                
                	 <!-- Slide 4 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/image-4.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Bienvenidos <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Creamos un entorno de seguridad </h2>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['490','490','490','490']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <!-- 
<p>Qualified Staff With Expertise in Services We Offer for more Resonable cost with love, Just explore about More!</p>
                        
 --></div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servicio/odontologia" class="theme-btn btn-style-one">Servicios</a>
                        </div>

                        <div class="tp-caption tp-resizeme big-ipad-hidden" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="shape"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['-85','-85','-65','-65']"
                        data-voffset="['0','0','0','0']"
                        data-x="['right','right','right','right']"
                        data-y="['bottom','bottom','bottom','bottom']"
                        data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <figure class="content-image"><img src="[base_url]theme/theme/images/main-slider/content-img-2.png" alt=""></figure>
                        </div>
                    </li>                  
                
                
                </ul>
            </div>
        </div>
    </section>
    <!--End Main Slider-->

    <!-- Call To Action -->
    <section class="call-to-action" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>¿Quieres trabajar con nosotros?</h2>
                    <p>Rellena el siguiente formulario y enseguida nos pondremos en contacto, ¡estaremos encantados! <span></span></p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacto</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call To Action -->

    <!-- About us-->
    <section class="about-us department-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="feature-column pull-right col-lg-7 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h2>¿Qué <span>ofrecemos?</span></h2>
                            <div class="separator"><span class="fa fa-plus"></span></div>
                            <!-- 
<p>A SedaClinic trobaràs un equip de professionals que et faran més fàcil tot el procés de sedació per als teus pacients. T'oferim servicios com:</p>
                        
 --></div>

                        <div class="row clearfix">
                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-stethoscope"></span></div>
                                    <h3><a href="[base_url]servicio/odontologia">Servicios</a></h3>
                                    <p>Médicos Licenciados especialistas en Anestesiología, Reanimación y Tratamiento del Dolor.
 </p>
                                </div>
                            </div>

                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-ambulance"></span></div>
                                    <h3><a href="[base_url]servicio/odontologia">Entorno legal</a></h3>
                                    <p>Profesionales cualificados, responsabilidad civil profesional, colegiación empresarial. </p>
                               
                                    
 </p>
                                </div>
                            </div>

                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-user-md"></span></div>
                                    <h3><a href="[base_url]servicio/odontologia">Soporte</a></h3>
                                    <p>Monitorización, oxigenoterapia, farmacología, desfibrilador DEA. Acreditación sedaciones. Formación personalizada.</div>
                            </div>

                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-medkit"></span></div>
                                    <h3><a href="[base_url]servicio/odontologia">Preparación del paciente</a></h3>
                                    <p>Información de los procedimientos y documentación reglamentaria. Optimización y soporte en la preparación del paciente.
 </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <!-- Image column -->
                <div class="image-column col-lg-5 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <figure><img src="[base_url]theme/theme/images/resource/image-2.png" alt=""></figure>
                    </div>
                </div>

                <!-- Info Column -->
                <div class="info-column col-md-12 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!-- About Block -->
                        <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>4,'blog.idioma'=>$_SESSION['lang']))->result() as $s): ?>
                        <div class="about-block col-md-4 col-sm-12 col-xs-12">
                            <div class="inner-box" style="min-height: 190px;">
                                <div class="icon-box">
                                    <img src="<?= base_url('img/blog/'.$s->icono) ?>" alt="">
                                </div>
                                <h4>
                                    <a href="<?= base_url('cursos/'.toURL($s->titulo)) ?>"><?= $s->titulo ?> </a>
                                </h4>
                                <p><?= $s->subtitulo ?></p>
                            </div>
                        </div>
                        <?php endforeach ?>                        

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->

    <!--Fluid Section One-->
    <section class="fluid-section-one">
        <div class="outer-container clearfix">
            <!--Content Column-->
            <div class="content-column">
                <div class="inner-box">
                    <div class="sec-title"> 
                        <h2>¿Por qué <span>nosotros?</span></h2>
                        <div class="separator"><span class="fa fa-plus"></span></div>
                    </div>
                    <div class="content">
                        <h4>Nuestro objectivo es ofrecer un servicio de excelencia para satisfacer al paciente y la clínica</h4>
                          </div>

                    <div class="choose-info row clearfix">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <ul class="services-list">
                            <h3 style=" font-size: 18px;">Pacientes</h3>
                                <li><a href="[base_url]servicio/odontologia">Somos especialistas en anestesia, sedación y de la analgesia</a></li>
                                <li><a href="[base_url]servicio/odontologia">Garantizamos tu seguridad y tu confort</a></li>
                                <li><a href="[base_url]servicio/odontologia">Te acompañamos en todo el proceso para que estés tranquilo</a></li>
                                 </ul>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <ul class="services-list">
                            <h3 style=" font-size: 18px;">Clínicas</h3>
                                <li><a href="[base_url]servicio/odontologia">Realiza tu procedimiento sin interferencias</a></li>
                                <li><a href="[base_url]servicio/odontologia">Aportamos los recursos humanos y materiales adecuados</a></li>
                                <li><a href="[base_url]servicio/odontologia">Obtén calidad asistencial y fideliza a tu paciente</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!--Image Column-->
            <div class="image-column" style="background-image:url([base_url]theme/theme/images/resource/image-1.jpg);">
                <figure class="image-box"><img src="[base_url]theme/theme/images/resource/image-1.jpg" alt=""></figure>
            </div>
        </div>
    </section>
    <!-- End Fluid Section -->

    <!-- Services Section -->
    <section class="services-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Nuestros <span>servicios</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div>

            <div class="row clearfix">
               <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>1,'blog.idioma'=>$_SESSION['lang']))->result() as $s): ?>
                    <!-- Service Block -->
                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="<?= base_url('img/blog/'.$s->icono) ?>" style="width:25%">
                            </div>
                            <h3><a href="<?= base_url('servicio/'.toUrl($s->titulo)) ?>"><?= $s->titulo ?></a></h3>
                            <p><?= cortar_palabras(strip_tags($s->texto),10) ?>...</p>
                        </div>
                    </div>
                <?php endforeach ?>
               
            </div>
        </div>
    </section>
    <!-- End Services Section -->

    <!-- Testmonial Section -->
    <section class="testimonial-section" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Qué opinan <span>nuestros clientes</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

            <div class="testimonial-carousel-two">
                <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>6,'blog.idioma'=>$_SESSION['lang'],'status'=>1))->result() as $b): ?>
                    <div class="testimonial-block-two">
                        <div class="inner-box">
                            <div class="thumb">
                                <img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="">
                                <?php if(!empty($b->foto2)): ?>
                                    <div class="clienteLogo">
                                        <img src="<?= base_url('img/blog/'.$b->foto2) ?>" alt="">
                                    </div>
                                <?php endif ?>
                            </div>
                            <h3 class="name"><?= $b->titulo ?><?= $b->titulo ?></h3>
                            <span class="address"><?= $b->subtitulo ?></span>
                            <p><?= strip_tags($b->texto) ?></p>
                        </div>
                    </div>
                <?php endforeach ?>


            </div>
        </div>
    </section>
    <!-- Testmonial Section -->

    <!-- Doctors Section -->
    <section class="doctors-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Nuestro <span>equipo</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Features Tab -->
            <div class="doctors-tabs tabs-box">

                <ul class="doctors-thumb tab-buttons clearfix">
                    <?php
                        $equipo = $this->db->get_where('blog',array('blog_categorias_id'=>3,'blog.idioma'=>$_SESSION['lang']));
                        foreach($equipo->result() as $n=>$e):
                    ?>

                        <li data-tab="#doctor-tab-<?= $n ?>" class="tab-btn <?= $n==0?'active-btn':''; ?>">
                            <div class="image-box">
                                <figure>
                                    <img src="<?= base_url('img/blog/'.$e->foto) ?>" alt="">
                                </figure>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <?php foreach($equipo->result() as $n=>$e): ?>
                        <div class="doctor-info tab <?= $n==0?'active-tab':'' ?>" id="doctor-tab-<?= $n ?>">
                            <div class="row clearfix">
                                <!-- Image-column -->
                                <div class="image-column col-md-4 col-sm-4 col-xs-12">
                                    <div class="inner-column">
                                        <div class="image-box">
                                            <a href="<?= base_url('img/blog/'.$e->foto) ?>" class="lightbox-image" data-fancybox="Gallery">
                                                <img src="<?= base_url('img/blog/'.$e->foto) ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Image-column -->
                                <div class="content-column col-md-8 col-sm-8 col-xs-12">
                                    <div class="inner-column">
                                        <h3 class="name" style="margin-bottom: 0; ">
                                            <a href="<?= base_url('equip/'.toURL($e->titulo)) ?>"><?= $e->titulo ?></a>
                                        </h3>
                                        <span class="designation"><?= $e->subtitulo ?></span>
                                                                              
                                        <div class="clearfix">
                                            <div class="social-links" style="float:none; ">
                                                <h4>Redes Sociales:</h4>
                                                <ul class="clearfix">
                                                    <?php foreach(explode(',',$e->tags) as $t): ?>
                                                        <li>                                        
                                                            <?php if(strpos($t,'twitter')): ?>
                                                                <a href="<?= trim($t) ?>">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            <?php elseif(strpos($t,'linkedin')): ?>
                                                                <a href="<?= trim($t) ?>">
                                                                    <i class="fa fa-linkedin"></i>
                                                                </a>
                                                            <?php else: ?>
                                                                <a href="mailto:<?= trim($t) ?>">
                                                                    <i class="fa fa-envelope"></i>
                                                                </a>
                                                            <?php endif ?>                                        
                                                        </li>
                                                        
                                                        
                                                    <?php endforeach ?>                                                     
                                                </ul>
                                                
                                            </div>
                                            
                                            <div class="call-btn" style="float:none; margin-top:30px;">
                                                <a href="<?= base_url('equip/'.toURL($e->titulo)) ?>" class="theme-btn btn-style-three">Leer más</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    

                </div>



            </div>

        </div>
    </section>
    <!-- End Doctors Section -->

    <!--Fun Facts Section-->
    <section class="fun-fact-section" style="background-image:url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-multiple-users-silhouette"></span>
                        <span class="count-text" data-speed="3000" data-stop="2350">0</span>
                        <h4 class="counter-title">Clientes satisfechos</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-people-1"></span>
                        <span class="count-text" data-speed="3000" data-stop="25">0</span>+
                        <h4 class="counter-title">Profesionales</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-success"></span>
                        <span class="count-text" data-speed="3000" data-stop="12000">0</span>+
                        <h4 class="counter-title">Operaciones</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-surgeon-doctor"></span>
                        <span class="count-text" data-speed="3000" data-stop="80">0</span>+
                        <h4 class="counter-title">Clínicas satisfechas</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Facts Section-->

    <!-- News Section -->
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title text-center"> 
                <h2>Últimas <span>noticias</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div>

            <div class="row clearfix">
                <!-- News Block -->
                [foreach:blog]
                    <div class="news-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="[link]"><img src="[foto]" alt=""></a>
                            </div>
                            <div class="content-box">
                                <h3><a href="#">[titulo]</a></h3>
                                <ul class="info">
                                    <li><a href="#">[user]c</a></li>
                                    <li><a href="#">[fecha]</a></li>
                                </ul>
                                <p>[texto]</p>
                            </div>
                        </div>
                    </div>
                [/foreach]
            </div>
        </div>
    </section>
    <!-- End News Section -->

    <!--Map Section-->
    <section class="map-section">
        <div class="inner-container clearfix" style="background-image: url([base_url]theme/theme/images/background/4.jpg); background-size:cover; height:50vh"></div>
    </section>
    <!--End Map Section-->

    <!-- Appointment Section -->
    <section class="appointment-section">
        <div class="auto-container">
            <div class="inner-container clearfix" style="background-image: url([base_url]theme/theme/images/background/4-2.jpg);">
                <div class="appointment-form">
                    <h2>Formulario de <span>contacto</span></h2>
                    <p>Rellena este formulario y nos pondremos en contacto contigo lo antes posible.</p>
                    <form method="post" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#response')">
                        <div class="row clearfix">
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="nombre" placeholder="Nombre y Apellidos" required="">
                            </div>
                            
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="email" name="email" placeholder="Email" required="">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="extras[empresa]" placeholder="Empresa">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="tel" name="extras[telefono]" placeholder="Teléfono" required="">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="extras[ciudad]" placeholder="Ciudad">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="extras[especialidad]" placeholder="Time" class="Especialidad">
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <textarea name="extras[mensaje]" placeholder="Mensaje"></textarea>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 checkbox">                                                                                    
                                <label for=""> 
                                    <input type="checkbox" value="1" name="politicas"> 
                                    He leído y acepto la <a href="<?= base_url('aviso-legal') ?>.html" target="_new">política de privacidad* </a>
                                </label>
                            </div>

                            <div id="response" class="col-md-12 col-sm-12 col-xs-12"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <button class="theme-btn btn-style-one" type="submit" name="submit-form">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Appointment -->

    <!--Clients Section-->
    <section class="clients-section">
        <div class="auto-container">
            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <?php $this->db->order_by('orden','ASC');foreach($this->db->get_where('socios')->result() as $s): ?>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="[base_url]theme/theme/images/clients/<?= $s->logo ?>" alt=""></a></figure></li>
                    <?php endforeach ?>                    
                </ul>
            </div>
        </div>
    </section>
    [footer]