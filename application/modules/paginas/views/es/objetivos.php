[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_6.jpg);">
    <div class="auto-container">
        <h1>Objectius</h1>
        <ul class="bread-crumb clearfix">
            <li><a href="[base_url]">Inicio </a></li>    
            <li>Objetivos</li>        
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Doctors Section -->
    <section class="department-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Los  <span>Pacientes</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Department Tab -->
            <div class="department-tabs tabs-box">
                <ul class="tab-buttons clearfix">
                    <li data-tab="#department-tab-1" class="tab-btn active-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-sleep"></span>
                            <h5>Sedación</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-2" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-life"></span>
                            <h5>Seguridad</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-3" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-yoga"></span>
                            <h5>Confort</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-4" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-deal"></span>
                            <h5>Confianza</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-5" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-smile"></span>
                            <h5>Satisfacción</h5>
                        </div>
                    </li>
                    <!-- 
<li data-tab="#department-tab-6" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-scientist"></span>
                            <h5>Laboratory</h5>
                        </div>
                    </li>
 -->
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <div class="department-info tab active-tab" id="department-tab-1">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-1.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-1.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Sedación</a></h3>
                                    <h5>Equipo de profesionales a tu servicio</h5>
                                    <p>Licenciados en Anestesiología y Reanimación. Aportamos un servicio de sedación, anestesia y analgesia de alta calidad.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-2">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-2.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-2.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Seguridad</a></h3>
                                    <h5>Nos preocupamos para que tu no te preocupes</h5>
                                    <p>Trabajamos con las condiciones humanas y materiales adecuadas. No asumas riesgos: trata con profesionales cualificados y expertos.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-3">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-3.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-3.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Confort</a></h3>
                                    <h5>Trabajamos para conseguir el máximo confort del paciente</h5>
                                    <p>Cualquier procedimiento o intervención comporta un estrés que condiciona una mala experiencia y es totalmente evitable. No dudes en ponerte en nuestras manos.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-4">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-4.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-4.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Confianza</a></h3>
                                    <h5>A tu lado desde el inicio hasta el final del proceso</h5>
                                    <p>Garantizamos una experiencia positiva. Te sentirás cuidado y acompañado en todo momento. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-5">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-5.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-5.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Satisfacción</a></h3>
                                    <h5>Nuestro objectivo, nuestra razón de ser</h5>
                                    <p>Los mejores resultados de un servicio profesional y humano con la máxima seguridad clínica. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-6">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-6.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-6.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->

    <!--Fun Facts Section-->
    <section class="fun-fact-section" style="background-image:url([base_url]theme/theme/images/background/3-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-multiple-users-silhouette"></span>
                        <span class="count-text" data-speed="3000" data-stop="2350">0</span>
                        <h4 class="counter-title">Clientes satisfechos</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-people-1"></span>
                        <span class="count-text" data-speed="3000" data-stop="25">0</span>+
                        <h4 class="counter-title">Profesionales</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-success"></span>
                        <span class="count-text" data-speed="3000" data-stop="12000">0</span>+
                        <h4 class="counter-title">Operaciones</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-surgeon-doctor"></span>
                        <span class="count-text" data-speed="3000" data-stop="80">0</span>+
                        <h4 class="counter-title">Clínicas satisfechas</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Facts Section-->















    <!-- Doctors Section -->
    <section class="department-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>La <span>Clínica</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Department Tab -->
            <div class="department-tabs tabs-box">
                <ul class="tab-buttons clearfix">
                    
                    <li data-tab="#department2-tab-2" class="tab-btn active-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-interview"></span>
                            <h5>Eficacia</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-3" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-medal"></span>
                            <h5>Calidad</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-4" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-justice"></span>
                            <h5>Seguridad</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-5" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-ecg"></span>
                            <h5>Externalización</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-1" class="tab-btn tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-friendship"></span>
                            <h5>Fidelización</h5>
                        </div>
                    </li>
                   <!-- 
 <li data-tab="#department2-tab-6" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-scientist"></span>
                            <h5>Laboratory</h5>
                        </div>
                    </li>
 -->
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <div class="department-info tab" id="department2-tab-1">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-7.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-7.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Fidelizació</a></h3>
                                    <h5>Un cliente satisfecho es la mejor publicidad</h5>
                                    <p>El mejor servicio y los mejores resultados garantizan una buena propaganda. Que hablen bien de ti!</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab active-tab" id="department2-tab-2">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-8.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-8.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Eficacia</a></h3>
                                    <h5>Procedimiento eficaz y sin interferencias</h5>
                                    <p>Cada procedimiento es único y requiere la máxima concentración. Trabaja en buenas condiciones para conseguir los mejores resultados. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-3">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-9.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-9.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Calidad</a></h3>
                                    <h5>En equipo, ofrecemos la mejor asistencia</h5>
                                    <p>Asegura un servicio de alta calidad. Gana competitividad, posicionamiento y atracción del cliente. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-4">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-10.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-10.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Seguridad</a></h3>
                                    <h5>Es una cuestión de responsabilidad</h5>
                                    <p>No pongas en riesgo a tu paciente y protege tu negocio (colegiación, responsabilidad civil, registro, documentación legal).</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-5">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-11.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-11.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Externalización y integración de servicios profesionales</a></h3>
                                    <h5>Déjalo todo en nuestras manos</h5>
                                    <p>Despreocupate de la compra de equipos, monitores y fármacos. Ofrecemos asesoramiento y soporte logístico según requerimientos.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Servicios</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-6">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-12.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-12.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->
    <!-- Call To Action -->
    <section class="call-to-action black" style="margin-top:50px; background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>¿Quieres trabajar con nostros?</h2>
                    <p>
                        Rellena el siguiente formulario y enseguida nos pondremos en contacto, ¡estaremos encantados! 
                    </p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacto</a>
                </div>
            </div>
        </div>
    </section>

[footer]