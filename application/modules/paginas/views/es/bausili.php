[menu]
<!--Page Title-->
    <section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_4.jpg);">
        <div class="auto-container">
            <h1>Dr. Marc Bausili</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url() ?>">Home </a></li>
                <li>Equip</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Doctor Detail Section -->
    <section class="doctor-detail">
        <div class="auto-container">
            <!-- Upper Box -->
            <div class="upper-box">
                <div class="row clearfix">
                    <div class="image-box col-md-4 col-sm-12 col-xs-12">
                        <div class="image"><a href="[base_url]theme/theme/images/resource/doctor-detail.jpg" class="lightbox-image"><img src="[base_url]theme/theme/images/resource/doctor-detail.jpg" alt=""></a></div>
                    </div>
                    <div class="detail-column col-md-8 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <div class="info-header">
                                
                                <h3>Dr. Marc Bausili</h3>
                                <span class="designation">Anestesiologia i Reanimació</span>
                            </div>
                            <ul class="text">
                                <li>
                                    
                                    <p>Expert en Sedacions en medicina plàstica, pacients pediàtrics i d’alt risc.</p>


<p>Doctor en Medicina i Cirurgia per la Universitat de Barcelona (UB).</p>
<p>Llicenciat en Anestesiologia i Reanimació. Hospital de la Santa Creu i Sant Pau (Barcelona).</p>
<p>Diplomat Europeu en Anestesiologia i Medicina Crítica per European Society of Anaesthesia (ESA).</p>
<p>&nbsp;</p><p>&nbsp;</p>

<p>Instructor en Suport Vital Bàsic i DEA, en Suport Vital Immediat i Suport Vital Avançat per l’European Ressuscitation Counci (ERC).</p>
<p>Programa Gestió de Persones i Equips d’Alt Rendiment (ESADE).</p>
<p>Membre Junta de la Societat Catalana Anestesiologia, Reanimació i Terapèutica del Dolor (SCARTD).</p>

                                    
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Lower Content -->
            <div class="upper-box">
                <div class="row clearfix">
                    <!-- Info Column -->
                    <div class="info-column col-md-4 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <!-- 
<ul class="contact-list">
                                <li><span>E-mail:</span> marcbausili@sedaclinic.com</li>
                                <!~~ 
<li><span>Phone: </span> +1 256 254 84 56</li>
                                <li><span>Timming:</span> 9:00am -5:00pm</li>
                                <li><span><a href="#" class="theme-btn btn-style-one">On Vacation</a></span> 12:00pm -3:00pm</li>
 ~~>
                            </ul>
 -->

                            <ul class="social-icon-one">
                                <li><a href="mailto:marcbausili@sedaclinic.com"><i class="fa fa-envelope"></i></a></li>
							<li><a href="https://twitter.com/marc_bausili"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.linkedin.com/in/marcbausiliribera"><i class="fa fa-linkedin"></i></a></li>
                            </ul>

                            
                        </div>
                    </div>

                    <!-- Form Column -->
                    <div class="form-column detail-column col-md-8 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <div class="appointment-form">
                                <h2>Formulari de <span>contacte</span></h2>
                                <p>Envia'm la teva consulta i et respondré el més aviat possible</p>
                                <form method="post" action="appointment.html">
                                    <div class="row clearfix">
                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <input type="text" name="username" placeholder="Nom i Cognom" required="">
                                            <span class="icon fa fa-user"></span>
                                        </div>
                                        
                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <input type="email" name="email" placeholder="Email" required="">
                                            <span class="icon fa fa-envelope"></span>
                                        </div>

                                        <!-- 
<div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <input type="tel" name="phone" placeholder="Telèfon" required="">
                                            <span class="icon fa fa-volume-control-phone"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <select class="custom-select-box">
                                                <option>Cardiology Department</option>
                                                <option>Neurology Department</option>
                                                <option>Urology Department</option>
                                                <option>Gynecological Conditions</option>
                                                <option>Pediatric Department</option>
                                                <option>Laboratory Department</option>
                                            </select>
                                            <span class="icon fa fa-caret-down"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <input type="text" name="day" placeholder="Day">
                                            <span class="icon fa fa-calendar"></span>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <input type="text" name="time" placeholder="Time" class="timepicker">
                                            <span class="icon fa fa-clock-o"></span>
                                        </div>
 -->
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                            <textarea name="message" placeholder="Missatge"></textarea>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-12 form-group text-right">
                                            <button class="theme-btn btn-style-one" type="submit" name="submit-form">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctor Detail Section -->

    <!-- Call To Action -->
    <!-- 
<section class="call-to-action" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Health Care Center</h2>
                    <p>if you have any Emerangcy by health problem contact this <span>No. 035 687 9514</span> or contact form</p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>
 -->
    [footer]