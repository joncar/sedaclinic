<!-- Main Footer -->
    <footer class="main-footer">
        <div class="auto-container">
        
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget about-widget">
                                    <div class="footer-logo">
                                        <figure>
                                            <a href="[base_url]"><img src="[base_url]theme/theme/images/footer-logo.png" alt=""></a>
                                        </figure>
                                    </div>
                                    <div class="widget-content">
                                        <p>Somos un equipo de anestesiólogos que ofrecemos un servicio de sedación y anestesia de alta calidad, humano y con la máxima seguridad.</p>
                                        
                                    </div>
                                    <figure>
                                            <a href="[base_url]"><img src="[base_url]theme/theme/images/footer-consell.jpg" alt=""></a>
                                        </figure>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                    <h2 class="widget-title">Enlaces de interés</h2>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="[base_url]">Inicio</a></li>
                                            <li><a href="[base_url]equip.html">Equipo</a></li>
                                            <li><a href="[base_url]faqs.html">Faqs</a></li>
                                            <li><a href="[base_url]blog.html">Blog</a></li>
                                            <li><a href="[base_url]contacte.html">Contacto</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>         
                        </div>
                    </div>
                    
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget contact-widget">
                                    <h2 class="widget-title">Contacto</h2>
                                    <div class="widget-content">
                                        <ul class="contact-list">
                                            <li><strong>Email: </strong><a href="mailto:info@sedaclinic.com">info@sedaclinic.com</a></li>
                                            <li><strong>Teléfono Info: </strong><a  href="tel:+34659633984">+34 659 633 984 </a></li>
                                            <li>

                                                <strong>Web: </strong>
                                                <a href="http://www.sedaclinic.com">www.sedaclinic.com</a>
                                            </li>
                                        </ul>
                                        <ul class="social-icon-three">
                                            <li><a href="https://www.linkedin.com/in/sedaclinic"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="https://twitter.com/sedaclinic"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="https://www.instagram.com/sedaclinic/"><i class="fa fa-instagram"></i></a></li>
                                            
                                        </ul>
                                    </div>
                                </div> 
                            </div>

                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget gallery-widget">
                                    <h2 class="widget-title">Instagram</h2>
                                    <div class="widget-content">
                                        <div class="outer clearfix social-feed-container">                                                                                        
                                        </div>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--Footer Bottom-->
         <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright-text clearfix">
                    <p>Sedaclínic © Todis los derechos reservados <br class="visible-xs" /><a href="[base_url]avis-legal.html">Aviso legal</a> | <a target="_new" href="http://www.jordimagana.com">by Jordi Magaña</a></p>
                    <div class="scroll-to-top scroll-to-target" data-target="html">
                        <span class="icon fa fa-arrow-circle-up"></span>
                    </div>
                </div> 
            </div>
        </div>
    </footer>
    <!-- End Main Footer -->