 [menu]
 <!--Page Title-->
    <section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_5.jpg);">
        <div class="auto-container">
            <h1>FAQ’s</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url() ?>">Inicio </a></li>
                <li>FAQ’s</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- FAQ's Section -->
    <section class="faq-section">
        <div class="inner-container">
            <div class="title">
                <h3>Preguntas frecuentes</h3>
                <p>Intentaremos resolver tus dudas con estas preguntas frecuentes. En caso de no encontrar solución en tu duda contacta con nosotros.</a></p>
            </div>

             <!-- FAQ's Tab -->
            <div class="faq-tabs tabs-box">

                <!--Tabs Container-->
                <div class="tabs-content">

                    <ul class="tab-buttons clearfix">
                        <li data-tab="#faq-tab-1" class="tab-btn active-btn">Anestesia y sedación</li>
                        <li data-tab="#faq-tab-2" class="tab-btn">Aspectos legales</li>
                        <li data-tab="#faq-tab-3" class="tab-btn">Seguridad</li>
                        
                    </ul>

                    <!--Tab / Active Tab-->
                    <div class="tab active-tab" id="faq-tab-1">
                        <div class="info-box">
                            <h3>¿Qué es la anestesia?</h3>
                            <p>Es la ausencia parcial o total de la sensibilidad, a una parte o en la totalidad del cuerpo, en este caso, provocada por una acción médica inducida mediante fármacos. El objetivo final es generar un estado de hipnosis controlada, inhibir el dolor y la conciencia.</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Qué es la sedación?</h3>
                            <p>Es un procedimiento médico que consiste en la administración de uno o varios medicamentos que producirán una disminución de la conciencia, de forma que el procedimiento al que es sometido será mejor soportado. Existen varios niveles de sedación, todos ellos tienen que estar debidamente controlados y realizados bajo responsabilidad de un anestesiólogo.

<br>• Sedación mínima o ansiolisi. El paciente responde normalmente a órdenes verbales, pudiendo estar atenuadas la función cognitiva y motora, sin alteración de los reflejos. Se puede conseguir cierto grado de amnesia.
<br>• Sedación moderada. Estado de depresión de la conciencia en el cual el paciente responde adecuadamente a órdenes solas o acompañadas con una leve estimulación. La respiración y función cardiovascular se mantienen inalteradas.
<br>• Sedación profunda. Estado controlado de depresión de la conciencia acompañada de pérdida parcial, habitualmente reversible, de los reflejos protectores y con respuesta a estímulos físicos. Solo es procedente en ciertas intervenciones invasivas.
</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Qué riesgos tiene una sedación?</h3>
                            <p>Lo más importante es saber que mientras dura el procedimiento, el paciente está bajo control y monitorización de un anestesiólogo. Trabajar con seguridad y el confort de un paciente relajado y tranquilo proporcionará las mejores condiciones para todo el equipo y usted será quien más se beneficiará.

<br><br>Si bien es cierto que la ausencia de riesgos no existe, como en cualquier actividad de la vida diaria. Aunque son infrecuentes pueden existir riesgos generales como son la presencia de sensación de mareo, náuseas y vómitos. También aquellos que pueden derivar de la canalización de una vía venosa para administrar fármacos (escozor, hematoma, etc.)

<br><br>También pueden existir otros riesgos específicos derivados de dolencias o alteraciones físicas, por lo cual se necesario que después de la consulta médica puedan ser evaluados, explicados y minimizados.
</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Existen contraindicaciones por someterse a una sedación?</h3>
                            <p>Todos los pacientes, después una valoración y autorización médica pueden ser sometidos a una sedación, puesto que permitirá la reducción de la ansiedad, un mejor confort durante el procedimiento, permitiendo que los profesionales puedan realizar con seguridad y mejores garantías su trabajo.

<br><br>Puede estar relativamente contraindicado en aquellos pacientes en los que se prevé una nula o escasa colaboración, como trastornos psiquiátricos graves, déficits intelectuales o mentales, o procesos neurológicos degenerativos. En el caso de niños, habrá que tener especial consideración las edades inferiores a 4 años.

<br><br>Se precisa una consulta médica para descartar otras causas agudas o dolencias graves que pueden suponer un riesgo importante, y por tanto, se podrá considerar inadecuado realizar algún tipo de sedación.
</p>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="tab" id="faq-tab-2">
                        <div class="info-box">
                            <h3>¿Quién está acreditado para realizar una sedación?</h3>
                            <p>La sedación solo puede ser realizada por un médico especialista en Anestesiología y Reanimación, no solo por el control y pericia en el procedimiento, sino sobre todo por sus competencias en el diagnóstico y tratamiento de riesgos y complicaciones.

<br><br>Solicita siempre la información sobre quien realizará la anestesia o sedación y su constancia en los documentos legales y consentimientos informados.
 </p>
                        </div>

                        <div class="info-box">
                            <h3>¿Qué requisitos profesionales exigimos?</h3>
                            <p>Aunque el objetivo primero es disponer de un equipo médico de prestigio, con altas capacidades técnicas y humanas, es de obligatorio cumplimiento que cada profesional:

<br>• Sea Especialista en Anestesiología y Reanimación vía MIR
<br>• Esté colegiado
<br>• Disponga de Póliza de Responsabilidad Profesional
<br>

</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Qué requisitos garantiza Sedaclínic?</h3>
                            <p>Sedaclinic es la marca empresarial de REANIMA SLP, una empresa de salud creada el 2014 con amplia experiencia en el campo de servicios de Anestesiología, Reanimación, formación sanitaria y asistencia médica a acontecimientos.
<br>• Inscrita en el Registro de Sociedades Profesionales
<br>• Dispone de Póliza de Responsabilidad Profesional
<br>• Certificado Obligaciones Tributarias
</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Como conseguir ser una clínica con autorización para realizar sedaciones?</h3>
                            <p>El Departament de Salut de la Generalitat exige unos requisitos para poder realizar sedaciones en tu clínica. Se trata de una serie de criterios estructurales y materiales, pero también de certificación profesional. Te facilitaremos el trabajo y te apoyaremos para conseguir la acreditación. </p>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="tab" id="faq-tab-3">
                        <div class="info-box">
                            <h3>¿Quién es el anestesiólogo?</h3>
                            <p>Es un profesional licenciado en Medicina y especialista en Anestesiología y Reanimación. Se trata de un médico formado específicamente no solo en el tratamiento de la anestesia en cualquier procedimiento o cirugía, sino también el responsable del tratamiento del dolor, de garantizar la seguridad y velar por tu salud en todo el proceso. Como especialista en reanimación tiene que asegurar la optimización de tu estado de salud para minimizar los riesgos, y sobre todo resolver con eficacia aquellas posibles complicaciones que puedan aparecer como consecuencia de la cirugía, de la anestesia o por descompensación de alguna dolencia existente.
</p>
                        </div>

                        <div class="info-box">
                            <h3>Son seguros los fármacos anestésicos?</h3>
                            <p>Actualmente se utilizan fármacos sedantes y analgésicos con una dosificación muy controlada, tienen un metabolismo rápido, esperable y con pocos efectos indeseables, y con un gran margen de seguridad. Por éste y otros motivos, hoy en día, las anestesias son más bien toleradas, personalizadas y seguras, siempre evidentemente en manos expertas.</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Son seguras las sedaciones?</h3>
                            <p>Se garantiza la máxima seguridad derivada del conocimiento técnico y la monitorización de los parámetros necesarios, según el paciente y el procedimiento. El mejor control pasa por el análisis de constantes vitales (oxigenación, tensión arterial, frecuencia cardíaca) y otros parámetros como la profundidad de la anestesia. Algunos pacientes y según los procedimientos precisan otros registros más estrictos (control de la glucosa, temperatura corporal, electrocardiograma, etc.).</p>
                        </div>

                        <div class="info-box">
                            <h3>¿Qué puedo hacer para garantizar la máxima seguridad en la sedación?</h3>
                            <p>Antes de someterse a una sedación es imprescindible una valoración integral por la anestesiólogos. Hay que valorar el historial médico completo (antecedentes médicos, medicación crónica, descompensaciones), realizar una exploración física y en algunos casos la realización de alguna prueba complementaria (electrocardiograma, analítica, etc.). También hay que ser estricto en las pautas y recomendaciones establecidas en la preparación previa (ayunas, medicación, etc.).
</p>
                        </div>
                    </div>

                                    </div>
            </div>
        </div>
    </section>
    <!--End FAQ's Section -->

    <!-- Call To Action -->
    <section class="call-to-action black" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>¿Quieres trabajar con nosotros?</h2>
                    <p>Rellena el siguiente formulario y enseguida nos pondremos en contacto, ¡estaremos encantados!

</p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacto</a>
                </div>
            </div>
        </div>
    </section>
    [footer]