[menu]
<!--Page Title-->
    <section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_1.jpg);">
        <div class="auto-container">
            <h1>Notícies</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="[base_url]">Inici </a></li>
                <li>Notícies</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- News Section -->
    <section class="news-section alternate">
        <div class="auto-container">

            <div class="row clearfix">
                <!-- News Block -->
                
                [foreach:blog]
                    <div class="news-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="[link]"><img src="[foto]" alt=""></a>
                            </div>
                            <div class="content-box">
                                <h3><a href="[link]">[titulo]</a></h3>
                                <ul class="info">
                                    <li>Per <a href="[link]">[user]</a></li>
                                    <li><a href="[link]">[fecha]</a></li>
                                </ul>
                                <p>[texto]</p>
                            </div>
                        </div>
                    </div>
                [/foreach]
                
            </div>
        </div>
    </section>
    <!-- End News Section -->

    <!-- Call To Action -->
    <section class="call-to-action black" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Vols treballar amb nosaltres?</h2>
                    <p>Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats! </p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call To Action -->
    [footer]