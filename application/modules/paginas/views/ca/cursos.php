[menu]
<!--Page Title-->
    <section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_7.jpg);">
        <div class="auto-container">
            <h1>Cursos</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="<?= base_url() ?>">Inici </a></li>
                <li>Cursos</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->


    <!-- Department Detail Section -->
    <div class="department-detail-section">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side pull-right col-md-8 col-sm-12 col-xs-12">
                    <div class="department-detail">
                        <div class="image-box">
                            <figure>
                                <img src="[base_url]img/blog/<?= $detail->foto ?>" alt="">
                            </figure>
                        </div> 

                        <div class="lower-content">
                            <h2><?= $detail->titulo ?></h2>
                            <h5><?= $detail->subtitulo ?></h5>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $detail->texto ?>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <img src="[base_url]img/blog/<?= $detail->foto2 ?>" alt="">
                                </div>
<div class="content-side pull-right col-md-6 col-sm-12 col-xs-12">
                            <div class="timetable-column">
                    <div class="timetable">
                        <h3><small>Et podem ajudar?</small></h3>
                        <!-- <p>Contacta amb nosaltres via mail i et respondrem lo abans possible.</p> -->
                        
                        <a href="[base_url]contacte.html" class="theme-btn btn-style-one"><i class="fa fa-user-md"></i> Contacte</a>
                    </div>
                    </div>
                </div>
                            </div>
                        </div>
                        
                    </div><!-- Service Detail -->
                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar department-sidebar">

                        <!-- Services Cat List -->
                        <div class="sidebar-widget categories">
                            <h3>Cursos</h3>
                            <ul class="department-list">
                                
                                <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>4,'blog.idioma'=>$_SESSION['lang']))->result() as $b): ?>
                                <li <?= $b->id==$detail->id?'class="active"':'' ?>><a href="[base_url]cursos/<?= toUrl($b->titulo) ?>"><?= $b->titulo ?></a></li>
                                <?php endforeach ?>
                            </ul>
                            
                        </div>
            
                        <!-- Help widgets -->

                        


                    </aside>
                    
                </div>
            </div>
        </div>
        
    </div>
    <!-- End Service Detail Section -->

    <section class="department-section alternate">
        <div class="auto-container">
            <div class="row clearfix">
                <!-- Department Block -->
                

                

            </div>
        </div>
    </section>




    <!-- Call To Action -->
    <section class="call-to-action black" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Vols treballar amb nosaltres?</h2>
                    <p>
                        Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats! 
                    </p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>
[footer]