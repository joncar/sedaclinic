[menu]

<!--Main Slider-->
    <section class="main-slider">
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>
                    <!-- Slide 1 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1684" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/sl2.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Benvinguts <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Professionals de la sedació experta</h2>
                        </div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servei/odontologia" class="theme-btn btn-style-one">Serveis</a>
                        </div>
                    </li> 
                    <!-- Slide 1 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1685" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/1.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Benvinguts <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>T'aportem tranquil·litat</h2>
                        </div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servei/odontologia" class="theme-btn btn-style-one">Serveis</a>
                        </div>
                    </li> 

                    <!-- Slide 2 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1686" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/image-2.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Benvinguts <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Garantim una experiència positiva </h2>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['490','490','490','490']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <!-- 
<p>Qualified Staff With Expertise in Services We Offer for more Resonable cost with love, Just explore about More!</p>
                        
 --></div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servei/odontologia" class="theme-btn btn-style-one">Serveis</a>
                        </div>

                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="shape"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['-85','-85','-550','-672']"
                        data-voffset="['0','0','0','0']"
                        data-x="['right','right','right','right']"
                        data-y="['bottom','bottom','bottom','bottom']"
                        data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <figure class="content-image"><img src="[base_url]theme/theme/images/main-slider/content-img-1.png" alt=""></figure>
                        </div>
                    </li>                  
                
                	 <!-- Slide 3 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-3.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/image-3.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Benvinguts <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Acompanyem al pacient</h2>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['490','490','490','490']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <!-- 
<p>Qualified Staff With Expertise in Services We Offer for more Resonable cost with love, Just explore about More!</p>
                        
 --></div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['70','70','70','70']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servei/odontologia" class="theme-btn btn-style-one">Serveis</a>
                        </div>

                        <!--<div class="tp-caption tp-resizeme big-ipad-hidden" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="shape"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['-85','-85','-65','-65']"
                        data-voffset="['0','0','0','0']"
                        data-x="['right','right','right','right']"
                        data-y="['bottom','bottom','bottom','bottom']"
                        data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <figure class="content-image"><img src="[base_url]theme/theme/images/main-slider/content-img-2.png" alt=""></figure>
                        </div>-->
                    </li>                  
                
                	 <!-- Slide 4 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="[base_url]theme/theme/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="[base_url]theme/theme/images/main-slider/image-4.jpg"> 
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['1050','800','600','500']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['-75','-105','-105','-105']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h5>Benvinguts <span>a</span> Sedaclínic</h5>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['400','800','600','800']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['-20','-40','-40','-50']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"delay":100,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Creem un entorn de seguretat </h2>
                        </div>

                        <div class="tp-caption " 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['490','490','490','490']"
                        data-whitespace="normal"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <!-- 
<p>Qualified Staff With Expertise in Services We Offer for more Resonable cost with love, Just explore about More!</p>
                        
 --></div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-hoffset="['50','50','50','50']"
                        data-voffset="['60','40','40','40']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="[base_url]servei/odontologia" class="theme-btn btn-style-one">Serveis</a>
                        </div>

                        <div class="tp-caption tp-resizeme big-ipad-hidden" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="shape"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-width="none"
                        data-hoffset="['-85','-85','-65','-65']"
                        data-voffset="['0','0','0','0']"
                        data-x="['right','right','right','right']"
                        data-y="['bottom','bottom','bottom','bottom']"
                        data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <figure class="content-image"><img src="[base_url]theme/theme/images/main-slider/content-img-2.png" alt=""></figure>
                        </div>
                    </li>                  
                
                
                </ul>
            </div>
        </div>
    </section>
    <!--End Main Slider-->

    <!-- Call To Action -->
    <section class="call-to-action" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Vols treballar amb nosaltres?</h2>
                    <p>Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats! <span></span></p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call To Action -->

    <!-- About us-->
    <section class="about-us department-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="feature-column pull-right col-lg-7 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h2>Què <span>oferim?</span></h2>
                            <div class="separator"><span class="fa fa-plus"></span></div>
                            <!-- 
<p>A SedaClinic trobaràs un equip de professionals que et faran més fàcil tot el procés de sedació per als teus pacients. T'oferim serveis com:</p>
                        
 --></div>

                        <div class="row clearfix">
                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-stethoscope"></span></div>
                                    <h3><a href="[base_url]servei/odontologia">Serveis</a></h3>
                                    <p>Metges Llicenciats especialistes en Anestesiologia, Reanimació i Tractament del Dolor.
 </p>
                                </div>
                            </div>

                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-ambulance"></span></div>
                                    <h3><a href="[base_url]servei/odontologia">Entorn legal</a></h3>
                                    <p>Professionals qualificats, responsabilitat civil professional, col·legiació empresarial. </p>
                               
                                    
 </p>
                                </div>
                            </div>

                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-user-md"></span></div>
                                    <h3><a href="[base_url]servei/odontologia">Suport</a></h3>
                                    <p>Monitorització, oxigenoteràpia, farmacologia, desfibril·lador DEA. Acreditació sedacions. Formació personalitzada.</div>
                            </div>

                            <!-- Feature BLock -->
                            <div class="featrue-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="icon-box"><span class="fa fa-medkit"></span></div>
                                    <h3><a href="[base_url]servei/odontologia">Preparació del pacient</a></h3>
                                    <p>Informació dels procediments i documentació reglamentària. Optimització i suport en la preparació del pacient.
 </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <!-- Image column -->
                <div class="image-column col-lg-5 col-md-12 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <figure><img src="[base_url]theme/theme/images/resource/image-2.png" alt=""></figure>
                    </div>
                </div>

                <!-- Info Column -->
                <div class="info-column col-md-12 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!-- About Block -->
                        <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>4,'blog.idioma'=>$_SESSION['lang']))->result() as $s): ?>
                        <div class="about-block col-md-4 col-sm-12 col-xs-12">
                            <div class="inner-box" style="min-height: 190px;">
                                <div class="icon-box">
                                    <img src="<?= base_url('img/blog/'.$s->icono) ?>" alt="">
                                </div>
                                <h4>
                                    <a href="<?= base_url('cursos/'.toURL($s->titulo)) ?>"><?= $s->titulo ?> </a>
                                </h4>
                                <p><?= $s->subtitulo ?></p>
                            </div>
                        </div>
                        <?php endforeach ?>                        

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->

    <!--Fluid Section One-->
    <section class="fluid-section-one">
        <div class="outer-container clearfix">
            <!--Content Column-->
            <div class="content-column">
                <div class="inner-box">
                    <div class="sec-title"> 
                        <h2>Perquè <span>nosaltres?</span></h2>
                        <div class="separator"><span class="fa fa-plus"></span></div>
                    </div>
                    <div class="content">
                        <h4>El nostre objectiu és oferir un servei d’excel·lència per satisfer el pacient i la clínica</h4>
                          </div>

                    <div class="choose-info row clearfix">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <ul class="services-list">
                            <h3 style=" font-size: 18px;">Pacients</h3>
                                <li><a href="[base_url]servei/odontologia">Som especialistes d’anestèsia, sedació i de l’analgèsia</a></li>
                                <li><a href="[base_url]servei/odontologia">Garantim el teva seguretat i el teu confort</a></li>
                                <li><a href="[base_url]servei/odontologia">T’acompanyem en tot el procés perquè estiguis tranquil</a></li>
                                 </ul>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <ul class="services-list">
                            <h3 style=" font-size: 18px;">Clíniques</h3>
                                <li><a href="[base_url]servei/odontologia">Realitza el teu procediment sense interferències</a></li>
                                <li><a href="[base_url]servei/odontologia">Aportem els recursos humans i materials adequats</a></li>
                                <li><a href="[base_url]servei/odontologia">Obté qualitat assistencial i fidelitza el teu pacient</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!--Image Column-->
            <div class="image-column" style="background-image:url([base_url]theme/theme/images/resource/image-1.jpg);">
                <figure class="image-box"><img src="[base_url]theme/theme/images/resource/image-1.jpg" alt=""></figure>
            </div>
        </div>
    </section>
    <!-- End Fluid Section -->

    <!-- Services Section -->
    <section class="services-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Els nostres <span>serveis</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div>

            <div class="row clearfix">
               <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>1,'blog.idioma'=>$_SESSION['lang']))->result() as $s): ?>
                    <!-- Service Block -->
                    <div class="service-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="<?= base_url('img/blog/'.$s->icono) ?>" style="width:25%">
                            </div>
                            <h3><a href="<?= base_url('servei/'.toUrl($s->titulo)) ?>"><?= $s->titulo ?></a></h3>
                            <p><?= cortar_palabras(strip_tags($s->texto),10) ?>...</p>
                        </div>
                    </div>
                <?php endforeach ?>
               
            </div>
        </div>
    </section>
    <!-- End Services Section -->

    <!-- Testmonial Section -->
    <section class="testimonial-section" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Què opinen <span>els nostres clients</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

            <div class="testimonial-carousel-two">
                <!-- Testimonial Block Two -->
                <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>6,'blog.idioma'=>$_SESSION['lang'],'status'=>1))->result() as $n=>$b): ?>
                    <div class="testimonial-block-two">
                        <div class="inner-box">
                            <div class="thumb">
                                <img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="">
                                <?php if(!empty($b->foto2)): ?>
                                    <div class="clienteLogo">
                                        <img src="<?= base_url('img/blog/'.$b->foto2) ?>" alt="">
                                    </div>
                                <?php endif ?>
                            </div>
                            <h3 class="name"><?= $b->titulo ?></h3>
                            <span class="address"><?= $b->subtitulo ?></span>
                            <p><?= strip_tags($b->texto) ?></p>
                        </div>
                    </div>
                <?php endforeach ?>
                


            </div>
        </div>
    </section>
    <!-- Testmonial Section -->

    <!-- Doctors Section -->
    <section class="doctors-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>El nostre <span>equip</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Features Tab -->
            <div class="doctors-tabs tabs-box">

                <ul class="doctors-thumb tab-buttons clearfix">
                    <?php
                        $equipo = $this->db->get_where('blog',array('blog_categorias_id'=>3,'blog.idioma'=>$_SESSION['lang']));
                        foreach($equipo->result() as $n=>$e):
                    ?>

                        <li data-tab="#doctor-tab-<?= $n ?>" class="tab-btn <?= $n==0?'active-btn':''; ?>">
                            <div class="image-box">
                                <figure>
                                    <img src="<?= base_url('img/blog/'.$e->foto) ?>" alt="">
                                </figure>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <?php foreach($equipo->result() as $n=>$e): ?>
                        <div class="doctor-info tab <?= $n==0?'active-tab':'' ?>" id="doctor-tab-<?= $n ?>">
                            <div class="row clearfix">
                                <!-- Image-column -->
                                <div class="image-column col-md-4 col-sm-4 col-xs-12">
                                    <div class="inner-column">
                                        <div class="image-box">
                                            <a href="<?= base_url('img/blog/'.$e->foto) ?>" class="lightbox-image" data-fancybox="Gallery">
                                                <img src="<?= base_url('img/blog/'.$e->foto) ?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Image-column -->
                                <div class="content-column col-md-8 col-sm-8 col-xs-12">
                                    <div class="inner-column">
                                        <h3 class="name" style="margin-bottom: 0; ">
                                            <a href="<?= base_url('equip/'.toURL($e->titulo)) ?>"><?= $e->titulo ?></a>
                                        </h3>
                                        <span class="designation"><?= $e->subtitulo ?></span>
                                                                              
                                        <div class="clearfix">
                                            <div class="social-links" style="float:none; ">
                                                <h4>Xarxes Socials:</h4>
                                                <ul class="clearfix">
                                                    <?php foreach(explode(',',$e->tags) as $t): ?>
                                                        <li>                                        
                                                            <?php if(strpos($t,'twitter')): ?>
                                                                <a href="<?= trim($t) ?>">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            <?php elseif(strpos($t,'linkedin')): ?>
                                                                <a href="<?= trim($t) ?>">
                                                                    <i class="fa fa-linkedin"></i>
                                                                </a>
                                                            <?php else: ?>
                                                                <a href="mailto:<?= trim($t) ?>">
                                                                    <i class="fa fa-envelope"></i>
                                                                </a>
                                                            <?php endif ?>                                        
                                                        </li>
                                                        
                                                        
                                                    <?php endforeach ?>                                                     
                                                </ul>
                                                
                                            </div>
                                            
                                            <div class="call-btn" style="float:none; margin-top:30px;">
                                                <a href="<?= base_url('equip/'.toURL($e->titulo)) ?>" class="theme-btn btn-style-three">LLegir més</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    

                </div>



            </div>

        </div>
    </section>
    <!-- End Doctors Section -->

    <!--Fun Facts Section-->
    <section class="fun-fact-section" style="background-image:url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-multiple-users-silhouette"></span>
                        <span class="count-text" data-speed="3000" data-stop="2350">0</span>
                        <h4 class="counter-title">Clients satisfets</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-people-1"></span>
                        <span class="count-text" data-speed="3000" data-stop="25">0</span>+
                        <h4 class="counter-title">Professionals</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-success"></span>
                        <span class="count-text" data-speed="3000" data-stop="12000">0</span>+
                        <h4 class="counter-title">Operacions</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-surgeon-doctor"></span>
                        <span class="count-text" data-speed="3000" data-stop="80">0</span>+
                        <h4 class="counter-title">Clíniques satisfetes</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Facts Section-->

    <!-- News Section -->
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title text-center"> 
                <h2>Últimes <span>notícies</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div>

            <div class="row clearfix">
                <!-- News Block -->
                [foreach:blog]
                    <div class="news-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="[link]"><img src="[foto]" alt=""></a>
                            </div>
                            <div class="content-box">
                                <h3><a href="#">[titulo]</a></h3>
                                <ul class="info">
                                    <li><a href="#">[user]c</a></li>
                                    <li><a href="#">[fecha]</a></li>
                                </ul>
                                <p>[texto]</p>
                            </div>
                        </div>
                    </div>
                [/foreach]
            </div>
        </div>
    </section>
    <!-- End News Section -->

    <!--Map Section-->
    <section class="map-section">
        <div class="inner-container clearfix" style="background-image: url([base_url]theme/theme/images/background/4.jpg); background-size:cover; height:50vh"></div>
    </section>
    <!--End Map Section-->

    <!-- Appointment Section -->
    <section class="appointment-section">
        <div class="auto-container">
            <div class="inner-container clearfix" style="background-image: url([base_url]theme/theme/images/background/4-2.jpg);">
                <div class="appointment-form">
                    <h2>Formulari de <span>contacte</span></h2>
                    <p>Omple aquest formulari i ens posarem en contacte amb tu lo abans possible.</p>
                    
                    <form method="post" action="paginas/frontend/contacot" onsubmit="return sendForm(this,'#response');">
                        <div class="row clearfix">
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="nombre" placeholder="Nom i Cognoms" required="">
                            </div>
                            
                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="email" name="email" placeholder="Email" required="">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="extras[empresa]" placeholder="Empresa">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="tel" name="extras[telefon]" placeholder="Telèfon" required="">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="extras[ciutat]" placeholder="Ciutat">
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                <input type="text" name="extras[especialitat]" placeholder="Especialitat">
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <textarea name="extras[missatge]" placeholder="Missatge"></textarea>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 checkbox">                                                                                    
                                <label for=""> <input type="checkbox" value="1" name="politicas"> He llegit i accepto la <a href="<?= base_url('avis-legal') ?>.html" target="_new">política de privacitat* </a></label>                                                                                    
                            </div>

                            <div id="response" class="col-md-12 col-sm-12 col-xs-12"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <button class="theme-btn btn-style-one" type="submit" name="submit-form">Enviar</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </section>
    <!-- End Appointment -->

    <!--Clients Section-->
    <section class="clients-section">
        <div class="auto-container">
            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <?php $this->db->order_by('orden','ASC');foreach($this->db->get_where('socios')->result() as $s): ?>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="[base_url]theme/theme/images/clients/<?= $s->logo ?>" alt=""></a></figure></li>
                    <?php endforeach ?>  
                </ul>
            </div>
        </div>
    </section>
    [footer]