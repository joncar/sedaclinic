[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_6.jpg);">
    <div class="auto-container">
        <h1>Avís legal</h1>
        <ul class="bread-crumb clearfix">
            <li><a href="[base_url]">Inici </a></li>    
            <li>Avís legal</li>        
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Doctors Section -->
    <section class="department-section" style="padding-bottom: 0">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Avís legal</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div>
            <div>
				<h4>Qui és el responsable del tractament de les seves dades personals?</h4> <br>
				<p>REANIMA SLP és el responsable del tractament de les dades personals que vostè ens proporciona i es responsabilitza de les referides dades personals d'acord amb la normativa aplicable en matèria de protecció de dades. 
				<br>REANIMA SLP amb domicili social a carrer Avda Indústria 53 08960 Sant Just Desvern; amb CIF número B-66312695 i inscrita en el Registre Mercantil de Barcelona.
				<br>E-Mail de contacte: info@sedaclinic.com
				</p>
				<br><br><h4>On emmagatzemem les seves dades?</h4> <br>
				<p>Les dades que recopilem sobre vostè s'emmagatzemen dins de l'Espai Econòmic Europeu («EEE»). Qualsevol transferència de les seves dades personals serà realitzada de conformitat amb les lleis aplicables.
				</p>
				<br><br><h4>¿A qui comuniquem les seves dades?</h4> <br>
				<p>Les seves dades poden ser compartides per l'empresa REANIMA SLP. Mai passem, venem ni intercanviem les seves dades personals amb terceres parts. Les dades que s'enviïn a tercers, s'utilitzaran únicament per oferir-li a vostè els nostres serveis. Li detallarem les categories de tercers que accedeixen a les seves dades per a cada activitat de tractament específica.
				</p>
				<br><br><h4>¿Quina és la base legal per al tractament de les seves dades personals? </h4> <br>
				<p>En cada tractament específic de dades personals recopilades sobre vostè, li informarem si la comunicació de dades personals és un requisit legal o contractual, o un requisit necessari per subscriure un contracte, i si està obligat a facilitar les dades personals, així com de les possibles conseqüències de no facilitar tals dades.
				</p>
				<br><br><h4>¿Quins són els seus drets? </h4> <br>
				<h5>Dret d’accés: </h5> <br>
				<p>Qualsevol persona té dret a obtenir confirmació sobre si REANIMA SLP estem tractant dades personals que els concerneixin, o no. Pot contactar a info@sedaclinic.com que li remetrà les dades personals que tractem sobre vostè per email.
				</p>
				<br><h5>Dret de portabilitat: </h5> <br>
				<p>Sempre que REANIMA SLP processi les seves dades personals a través de mitjans automatitzats sobre la base del seu consentiment o a un acord, vostè té dret a obtenir una còpia de les seves dades en un format estructurat, d'ús comú i lectura mecànica transferida al seu nom o a un tercer. En ella s'inclouran únicament les dades personals que vostè ens hagi facilitat.
				</p>
				<br><h5>Dret de rectificació:&nbsp;&nbsp;</h5> <br>
				<p>Vostè té dret a sol·licitar la rectificació de les seves dades personals si són inexactes, incloent el dret a completar dades que figurin incomplets.
				</p>
				<br><h5>Dret de supressió:&nbsp;&nbsp;</h5> <br>
				<p>Vostè té dret a obtenir sense dilació indeguda la supressió de qualsevol dada personal seva tractada per REANIMA SLP a qualsevol moment, excepte en les següents situacions: <br> *té un deute pendent amb REANIMA SLP, independentment del mètode de pagament
				<br>* se sospita o està confirmat que ha utilitzat incorrectament els nostres serveis en els últims quatre anys
				<br>* ha contractat algun servei pel que conservarem les seves dades personals en relació amb la transacció per normativa comptable.

				</p>
				<br><h5>Dret d’oposició al tractament de dades en base a l’interés legítim: </h5> <br>
				<p>Vostè té dret a oposar-se al tractament de les seves dades personals sobre la base de l'interès legítim de REANIMA SLP. REANIMA SLP no seguirà tractant les dades personals tret que puguem acreditar motius legítims imperiosos per al tractament que prevalguin sobre els seus interessos, drets i llibertats, o bé per a la formulació, l'exercici o la defensa de reclamacions.
				</p>
				<p></p>
				<br><h5>Dret d’oposició al marketing directe: </h5> <br>
				<p>Vostè té dret a oposar-se al màrqueting directe, incloent l'elaboració de perfils realitzada per a aquest màrqueting directe.<br>
				Pot desvincular-se del màrqueting directe en qualsevol moment de les següents maneres: *seguint les indicacions facilitades a cada correu de màrqueting

				</p>
				<p></p>
				<br><h5>Dret a presentar una reclamació davant d’una autoritat de control: </h5> <br>
				<p>Si vostè considera que REANIMA SLP tracta les seves dades d'una manera incorrecta, pot contactar amb nosaltres. També té dret a presentar una queixa davant l'autoritat de protecció de dades competent.
				</p>
				<br><h5>Dret a limitació en el tractament:  </h5> <br>
				<p>Vostè té dret a sol·licitar que REANIMA SLP limiti el tractament de les seves dades personals en les següents circumstàncies:
				<br>* si vostè s'oposa al tractament de les seves dades sobre la base de l'interès legítim de REANIMA SLP. REANIMA SLP haurà de limitar qualsevol tractament d'aquestes dades a l'espera de la verificació de l'interès legítim.
				<br>* si vostè reclama que les seves dades personals són incorrectes, REANIMA SLP ha de limitar qualsevol tractament d'aquestes dades fins que es verifiqui la precisió dels mateixos.
				<br>* si el tractament és il·legal, vostè podrà oposar-se al fet que s'eliminin les dades personals i, en el seu lloc, sol·licitar la limitació del seu ús.
				<br>* si REANIMA SLP ja no necessita les dades personals, però vostè els necessita per a la formulació, l'exercici o la defensa de reclamacions.

				</p><br><br>
				<h4>Exercici de drets:  </h4> <br>
				<p>Ens prenem molt de debò la protecció de dades i, per tant, comptem amb personal de servei al client dedicat que s'ocupa de les seves sol·licituds en relació amb els drets abans esmentats. Sempre pot posar-se en contacte amb ells a: info@sedaclinic.com

				</p>
						</div>
        </div>
    </section>
    <!-- End Doctors Section -->

    
    <!-- Call To Action -->
    <section class="call-to-action" style="margin-top:50px; background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Vols treballar amb nosaltres?</h2>
                    <p>
                        Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats! 
                    </p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>

[footer]