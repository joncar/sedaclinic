<!-- Main Header-->
    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left">
                        <ul class="contact-list clearfix">                            
                            <li><i class="icon fa fa-phone"></i> <a  href="tel:+34659633984">+34 659 633 984 </a></li>
                            <li><i class="icon fa fa-envelope"></i> <a  href="mailto:info@sedaclinic.com<">info@sedaclinic.com </a></li>
                            <li class="hidden-xs">
                                <a  href="https://www.linkedin.com/in/sedaclinic/" target="_new"><i class="icon fa fa-linkedin"></i></a> 
                                <a  href="https://twitter.com/sedaclinic" target="_new"><i class="icon fa fa-twitter"></i></a>
                                <a  href="https://www.instagram.com/sedaclinic/" target="_new"><i class="icon fa fa-instagram"></i></a>
                            </li>
                            <li class="marca">
                                <div class="marquee"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="top-right hidden-xs">
                        <a href="<?= base_url('main/traduccion/es') ?>" class="theme-btn btn-style-one">ESP</a> |
                        <a href="<?= base_url('main/traduccion/ca') ?>" class="theme-btn btn-style-one"><u>CAT</u></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container">
                <div class="logo-box">
                    <div class="logo"><a href="<?= base_url() ?>"><img src="[base_url]theme/theme/images/logo1.png" alt="" title=""></a></div>
                </div>

                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="hidden-xs hidden-sm"><a href="[base_url]">Inici</a></li>
                                <li><a href="[base_url]objectius.html">Objectius</a></li>                                
                                <li class="dropdown"><a href="#">Serveis</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>1,'blog.idioma'=>$_SESSION['lang']))->result() as $b): ?>
                                        <li><a href="<?= base_url('servei/'.toUrl($b->titulo)) ?>"><?= $b->titulo ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                </li>
                                <li><a href="[base_url]equip.html">Equip</a></li>
                                <li><a href="[base_url]faqs.html">Faqs</a></li>
                                <li><a href="[base_url]blog">Blog</a></li>
                                <li><a href="[base_url]contacte.html">Contacte</a></li>
                                <li class="visible-xs"><a href="<?= base_url('main/traduccion/es') ?>">Español</a></li>

                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->

                    <!--Search Box-->
                    <div class="search-box-outer">
                        <!--Search Box-->
                        <div class="dropdown dropdown-outer">
                            <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                            <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                <li class="panel-outer">
                                    <div class="form-container">
                                        <form method="get" action="paginas/frontend/search">
                                            <div class="form-group">
                                                <input type="search" name="q" value="" placeholder="Buscar aqui" required="">
                                                <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!--End earch Box-->
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="<?= base_url() ?>" class="img-responsive"><img src="[base_url]theme/theme/images/logo.png" alt="" title=""></a>
                </div>
                    
                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><a href="[base_url]">Inici</a></li>
                                <li><a href="[base_url]objectius.html">Objectius</a></li>                                
                                <li class="dropdown"><a href="#">Serveis</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>1,'blog.idioma'=>$_SESSION['lang']))->result() as $b): ?>
                                        <li><a href="<?= base_url('servei/'.toUrl($b->titulo)) ?>"><?= $b->titulo ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                </li>
                                <li><a href="[base_url]equip.html">Equip</a></li>
                                <li><a href="[base_url]faqs.html">Faqs</a></li>
                                <li><a href="[base_url]blog">Blog</a></li>
                                <li><a href="[base_url]contacte.html">Contacte</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    </header>
    <!--End Main Header -->