[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_4.jpg);">
	<div class="auto-container">
		<h1>El nostre equip</h1>
		<ul class="bread-crumb clearfix">
			<li><a href="[base_url]">Home </a></li>
			<li>Equip</li>
		</ul>
	</div>
</section>
<!--End Page Title-->
<!-- Doctors Team Section -->
<section class="doctors-team">
	<div class="auto-container">
		<div class="row clearfix">
			<!-- Team Block -->
			<?php foreach($this->db->get_where('blog',array('blog_categorias_id'=>3,'blog.idioma'=>$_SESSION['lang']))->result() as $b): ?>
				<div class="team-block col-md-4 col-sm-6 col-xs-12">
					<div class="inner-box">
						<div class="image-box">
							<a href="<?= base_url('equip/'.toUrl($b->titulo)) ?>">
								<img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="">
							</a>
							<ul class="social-links">
							<li><a href="mailto:marcbausili@sedaclinic.com"><i class="fa fa-envelope"></i></a></li>
								<li><a href="https://twitter.com/marc_bausili"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://www.linkedin.com/in/marcbausiliribera"><i class="fa fa-linkedin"></i></a></li>								
							</ul>
						</div>
						<div class="info-box">
							<h3><a href="<?= base_url('equip/'.toUrl($b->titulo)) ?>"><?= $b->titulo ?></a></h3>
							<span class="designation"><?= $b->subtitulo ?></span>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>		
	</div>
</section>
<!-- End Doctors Team Section -->
<!-- Call To Action -->
<section class="call-to-action black" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
    <div class="auto-container">
        <div class="inner-container clearfix">
            <div class="title-box">
                <span class="icon flaticon-medical-2"></span>
                <h2>Vols treballar amb nosaltres?</h2>
                <p>Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats! </p>
            </div>
            <div class="btn-box">
                <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
            </div>
        </div>
    </div>
</section>
[footer]