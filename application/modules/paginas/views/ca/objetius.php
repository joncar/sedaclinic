[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_6.jpg);">
    <div class="auto-container">
        <h1>Objetius</h1>
        <ul class="bread-crumb clearfix">
            <li><a href="[base_url]">Inici </a></li>    
            <li>Objetius</li>        
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Doctors Section -->
    <section class="department-section" style="padding-bottom: 0">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Els  <span>Pacients</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Department Tab -->
            <div class="department-tabs tabs-box">
                <ul class="tab-buttons clearfix">
                    <li data-tab="#department-tab-1" class="tab-btn active-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-sleep"></span>
                            <h5>Sedació</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-2" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-life"></span>
                            <h5>Tranquil•litat</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-3" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-yoga"></span>
                            <h5>Confort</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-4" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-deal"></span>
                            <h5>Confiança</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-5" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-smile"></span>
                            <h5>Satisfacció</h5>
                        </div>
                    </li>
                    <!-- 
<li data-tab="#department-tab-6" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-scientist"></span>
                            <h5>Laboratory</h5>
                        </div>
                    </li>
 -->
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <div class="department-info tab active-tab" id="department-tab-1">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-1.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-1.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Sedació, amnèsia i analgèsia</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-2">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-2.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-2.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-3">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-3.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-3.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-4">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-4.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-4.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-5">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-5.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-5.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-6">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-6.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-6.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->

    <!--Fun Facts Section-->
    <section class="fun-fact-section" style="background-image:url([base_url]theme/theme/images/background/3-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-multiple-users-silhouette"></span>
                        <span class="count-text" data-speed="3000" data-stop="2350">0</span>
                        <h4 class="counter-title">Satisfied Patients</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-people-1"></span>
                        <span class="count-text" data-speed="3000" data-stop="205">0</span>+
                        <h4 class="counter-title">Doctor’s Team</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-success"></span>
                        <span class="count-text" data-speed="3000" data-stop="2125">0</span>
                        <h4 class="counter-title">Success Mission</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-surgeon-doctor"></span>
                        <span class="count-text" data-speed="3000" data-stop="257">0</span>+
                        <h4 class="counter-title">Successfull Surgeries</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Facts Section-->















    <!-- Doctors Section -->
    <section class="department-section"  style="padding-bottom: 0">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Les <span>Clíniques</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Department Tab -->
            <div class="department-tabs tabs-box">
                <ul class="tab-buttons clearfix">
                    <li data-tab="#department-tab-1" class="tab-btn active-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-friendship"></span>
                            <h5>Fidelització</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-2" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-interview"></span>
                            <h5>Eficacia</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-3" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-medal"></span>
                            <h5>Qualitat</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-4" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-justice"></span>
                            <h5>Seguretat</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-5" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-ecg"></span>
                            <h5>Externalització</h5>
                        </div>
                    </li>
                   <!-- 
 <li data-tab="#department-tab-6" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-scientist"></span>
                            <h5>Laboratory</h5>
                        </div>
                    </li>
 -->
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <div class="department-info tab active-tab" id="department-tab-1">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-7.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-7.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-2">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-8.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-8.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-3">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-9.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-9.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-4">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-10.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-10.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-5">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-11.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-11.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-6">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-12.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-12.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->

    <!--Fun Facts Section-->
    <section class="fun-fact-section" style="background-image:url([base_url]theme/theme/images/background/3-2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-multiple-users-silhouette"></span>
                        <span class="count-text" data-speed="3000" data-stop="2350">0</span>
                        <h4 class="counter-title">Satisfied Patients</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-people-1"></span>
                        <span class="count-text" data-speed="3000" data-stop="205">0</span>+
                        <h4 class="counter-title">Doctor’s Team</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-success"></span>
                        <span class="count-text" data-speed="3000" data-stop="2125">0</span>
                        <h4 class="counter-title">Success Mission</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="count-box">
                        <span class="icon flaticon-surgeon-doctor"></span>
                        <span class="count-text" data-speed="3000" data-stop="257">0</span>+
                        <h4 class="counter-title">Successfull Surgeries</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Facts Section-->


[footer]