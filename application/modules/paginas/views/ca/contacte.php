[menu]
<!--Page Title-->
    <section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_2.jpg);">
        <div class="auto-container">
            <h1>Contacte</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="[base_url]">Inici </a></li>
                <li>Contacte</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Contact Page Section -->
    <section class="contact-page-section">
        <div class="auto-container">
            

            <div class="form-section_erase">
                <div class="row clearfix">
                    <div class="contact-column col-md-4 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <span class="title">Informació de contacte</span>
                            <h3>Contacte</h3>
                            <h5>Dades:</h5>
                            <ul class="contact-info">
                                <li>
                                    <span class="fa fa-volume-control-phone"></span> 
                                    <p>+34 659 633 984</p>
                                    
                                </li>
                                <li><span class="fa fa-envelope"></span><a href="mailto:info@sedaclinic.com">info@sedaclinic.com</a></li>
                            </ul>
                            <h5>Xarxes Socials</h5>
                            <ul class="social-icon-one">
                                <li><a href="https://www.linkedin.com/in/sedaclinic"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="https://twitter.com/sedaclinic"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <!-- Form Column -->
                    <div class="form-column col-md-8 col-sm-12 col-xs-12">
                        <div class="contact-form">
                            <span class="title">Contacta amb nosaltres</span>
                            <h3>Formulari de contacte</h3>
                            <form method="post" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#response')">
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input type="text" name="nombre" placeholder="Nom" required="">
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input type="email" name="email" placeholder="Email" required="">
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input type="text" name="extras[Telefon]" placeholder="Telèfon" required="">
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input type="text" name="extras[Tema]" placeholder="Tema" required="">
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="extras[Missatge]" placeholder="Missatge"></textarea>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 checkbox">                                                                                    
                                            <label for=""> <input type="checkbox" value="1" name="politicas"> He llegit i accepto la <a href="<?= base_url('avis-legal') ?>.html" target="_new">política de privacitat* </a></label>                                                                                    
                                    </div>

                                    <div id="response" class="col-md-12 col-sm-12 col-xs-12"></div>
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group text-right">
                                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">Enviar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Contact Page Section -->    
[footer]