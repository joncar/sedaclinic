 [menu]
 <!--Page Title-->
    <section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_5.jpg);">
        <div class="auto-container">
            <h1>FAQ’s</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="[base_url]">Inici </a></li>
                <li>FAQ’s</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- FAQ's Section -->
    <section class="faq-section">
        <div class="inner-container">
            <div class="title">
                <h3>Preguntes freqüents</h3>
                <p>Intentarem resoldre els teus dubtes amb aquestes preguntes freqüents. En cas de no trobar solució al teu dubte contacta amb nosaltres.</a></p>
            </div>

             <!-- FAQ's Tab -->
            <div class="faq-tabs tabs-box">

                <!--Tabs Container-->
                <div class="tabs-content">

                    <ul class="tab-buttons clearfix">
                        <li data-tab="#faq-tab-1" class="tab-btn active-btn">Anestèsia i sedació</li>
                        <li data-tab="#faq-tab-2" class="tab-btn">Aspectes legals</li>
                        <li data-tab="#faq-tab-3" class="tab-btn">Seguretat</li>
                        
                    </ul>

                    <!--Tab / Active Tab-->
                    <div class="tab active-tab" id="faq-tab-1">
                        <div class="info-box">
                            <h3>Què és l'anestèsia?</h3>
                            <p>És l’absència parcial o total de la sensibilitat, a una part o en la totalitat del cos, en aquest cas, provocada per una acció mèdica induïda mitjançant fàrmacs. L’objectiu final és generar un estat d’hipnosi controlada, inhibir el dolor i la consciència.</p>
                        </div>

                        <div class="info-box">
                            <h3>Què és la sedació?</h3>
                            <p>És un procediment mèdic que consisteix en l’administració d’un o varis medicaments que produiran una disminució de la consciencia, de forma que el procediment al que és sotmès serà millor suportat. Existeixen diversos nivells de sedació, tots ells tenen que estar degudament controlats i realitzats sota responsabilitat d’un anestesiòleg.

<br>• Sedació mínima o ansiolisi. El pacient respon normalment a ordres verbals, podent estar atenuades la funció cognitiva i motora, sense alteració dels reflexes. Es pot aconseguir cert grau d’amnèsia.
<br>• Sedació moderada. Estat de depressió de la consciència en el qual el pacient respon adequadament a ordres soles o acompanyades amb una lleu estimulació. La respiració i funció cardiovascular es mantenen inalterades.
<br>• Sedació profunda. Estat controlat de depressió de la consciència acompanyat de pèrdua parcial, habitualment reversible, dels reflexes protectors i amb resposta a estímuls físics. Només és procedent en certes intervencions invasives.
</p>
                        </div>

                        <div class="info-box">
                            <h3>Quins riscos té una sedació?</h3>
                            <p>Allò més important és saber que mentre dura el procediment, el pacient està sota control i monitorització d’un anestesiòleg. Treballar amb seguretat i el confort d’un pacient relaxat i tranquil proporcionarà les millors condicions per tot l’equip i vostè serà qui més se’n beneficiarà.

<br><br>Si bé és cert que l’absència de riscos no existeix, com en qualsevol activitat de la vida diària. Encara que són infreqüents poden existir riscos generals com són la presència de sensació de mareig, nàusees i vòmits. També aquells que poden derivar de la canalització d’una via venosa per administrar fàrmacs (coïssor, hematoma, etc.)

<br><br>També poden existir altres riscos específics derivats de malalties o alteracions físiques, per la qual cosa es necessari que després de la consulta mèdica puguin ser avaluats, explicats i minimitzats.
</p>
                        </div>

                        <div class="info-box">
                            <h3>Existeixen contraindicacions per sotmetre’s a una sedació?</h3>
                            <p>Tots els pacients, després una valoració i autorització mèdica poden ser sotmesos a una sedació, ja que permetrà la reducció de l’ansietat, un millor confort durant el procediment, permetent que els professionals puguin realitzar amb seguretat i millors garanties la seva feina.

<br><br>Pot estar relativament contraindicat en aquells pacients en els que es preveu una nul·la o escassa col·laboració, com trastorns psiquiàtrics greus, dèficits intel·lectuals o mentals, o processos neurològics degeneratius. En el cas de nens, caldrà tenir especial consideració les edats inferiors a 4 anys.

<br><br>Es precisa una consulta mèdica per descartar altres causes agudes o malalties greus que poden suposar un risc important, i per tant, es podrà considerar inadequat realitzar algun tipus de sedació.
</p>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="tab" id="faq-tab-2">
                        <div class="info-box">
                            <h3>Qui està acreditat per realitzar una sedació?</h3>
                            <p>La sedació només pot ser realitzada per un metge especialista en Anestesiologia i Reanimació, no només pel control i expertesa en el procediment, sinó sobretot per les seves competències en el diagnòstic i tractament de riscos i complicacions.

<br><br>Sol·licita sempre la informació sobre qui realitzarà l’anestèsia o sedació i la seva constància en els documents legals i consentiments informats.
 </p>
                        </div>

                        <div class="info-box">
                            <h3>Quins requisits professionals exigim?</h3>
                            <p>Encara que l’objectiu primer és disposar d’un equip mèdic de prestigi, amb altes capacitats tècniques i humanes, és d’obligatori compliment que cada professional:

<br>• Sigui Especialista en Anestesiologia i Reanimació via MIR
<br>• Estigui col·legiat
<br>• Disposi de Pòlissa de Responsabilitat Professional
<br>

</p>
                        </div>

                        <div class="info-box">
                            <h3>Quins requisits garanteix Sedaclínic?</h3>
                            <p>Sedaclinic és la marca empresarial de REANIMA SLP, una empresa de salut creada el 2014 amb àmplia experiència en el camp de serveis d’Anestesiologia, Reanimació, formació sanitària i assistència mèdica a esdeveniments. 

<br>• Inscrita en el Registre de Societats Professionals
<br>• Disposa de Pòlissa de Responsabilitat Professional
<br>• Certificat Obligacions Tributàries
</p>
                        </div>

                        <div class="info-box">
                            <h3>Com aconseguir ser una clínica amb autorització per realitzar sedacions?</h3>
                            <p>El Departament de Salut de la Generalitat exigeix uns requisits per poder realitzar sedacions a la teva clínica. Es tracta d’una sèrie de criteris estructurals i materials, però també de certificació professional. Et facilitarem la feina i et donem suport per aconseguir l’acreditació. </p>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="tab" id="faq-tab-3">
                        <div class="info-box">
                            <h3>Qui és l’anestesiòleg?</h3>
                            <p>És un professional llicenciat en Medicina i especialista en Anestesiologia i Reanimació. Es tracta d’un metge format específicament no només en el tractament de l’anestèsia en qualsevol procediment o cirurgia, sinó també el responsable del tractament del dolor, de garantir la seguretat i vetllar per la teva salut en tot el procés. Com a especialista en reanimació ha d’assegurar la optimització del teu estat de salut per minimitzar els riscos, i sobretot resoldre amb eficàcia aquelles possibles complicacions que puguin aparèixer com a conseqüència de la cirurgia, de l’anestèsia o per descompensació d’alguna malaltia existent.
</p>
                        </div>

                        <div class="info-box">
                            <h3>Són segurs els fàrmacs anestèsics?</h3>
                            <p>Actualment s’utilitzen fàrmacs sedants i analgèsics amb una dosificació molt controlada, tenen un metabolisme ràpid, esperable i amb pocs efectes indesitjables, i amb un gran marge de seguretat. És per això, i per altres motius que, avui en dia, les anestèsies són més ben tolerades, personalitzades i segures, sempre evidentment en mans expertes.</p>
                        </div>

                        <div class="info-box">
                            <h3>Són segures les sedacions?</h3>
                            <p>Es garanteix la màxima seguretat derivada del coneixement tècnic i la monitorització dels paràmetres necessaris, segons el pacient i el procediment. El millor control passa per l’anàlisi de constants vitals (oxigenació, tensió arterial, freqüència cardíaca) i altres paràmetres com la profunditat de l’anestèsia. Alguns pacients i segons els procediments precisen altres registres més estrictes (control de la glucosa, temperatura corporal, electrocardiograma, etc.).</p>
                        </div>

                        <div class="info-box">
                            <h3>Què puc fer per garantir la màxima seguretat en la sedació?</h3>
                            <p>Abans de sotmetre’s a una sedació és imprescindible una valoració integral per l’anestesiòleg. Cal valorar l’historial mèdic complet (antecedents mèdics, medicació crònica, descompensacions), realitzar una exploració física i en alguns casos la realització d’alguna prova complementària (electrocardiograma, analítica, etc.). També cal ser estricte en les pautes i recomanacions establertes en la preparació prèvia (dejú, medicació, etc.).
</p>
                        </div>
                    </div>

                                    </div>
            </div>
        </div>
    </section>
    <!--End FAQ's Section -->

    <!-- Call To Action -->
    <section class="call-to-action black" style="background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Vols treballar amb nosaltres?</h2>
                    <p>Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats!

</p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>
    [footer]