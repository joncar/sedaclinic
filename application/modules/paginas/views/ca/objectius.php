[menu]
<!--Page Title-->
<section class="page-title" style="background-image:url([base_url]theme/theme/images/background/6_6.jpg);">
    <div class="auto-container">
        <h1>Objectius</h1>
        <ul class="bread-crumb clearfix">
            <li><a href="[base_url]">Inici </a></li>    
            <li>Objectius</li>        
        </ul>
    </div>
</section>
<!--End Page Title-->

<!-- Doctors Section -->
    <section class="department-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>Els  <span>Pacients</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Department Tab -->
            <div class="department-tabs tabs-box">
                <ul class="tab-buttons clearfix">
                    <li data-tab="#department-tab-1" class="tab-btn active-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-sleep"></span>
                            <h5>Sedació</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-2" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-life"></span>
                            <h5>Seguretat</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-3" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-yoga"></span>
                            <h5>Confort</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-4" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-deal"></span>
                            <h5>Confiança</h5>
                        </div>
                    </li>
                    <li data-tab="#department-tab-5" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-smile"></span>
                            <h5>Satisfacció</h5>
                        </div>
                    </li>
                    <!-- 
<li data-tab="#department-tab-6" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-scientist"></span>
                            <h5>Laboratory</h5>
                        </div>
                    </li>
 -->
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <div class="department-info tab active-tab" id="department-tab-1">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-1.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-1.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Sedació</a></h3>
                                    <h5>Equip de professionals al teu servei</h5>
                                    <p>Llicenciats en Anestesiologia i Reanimació. Aportem un servei de sedació, anestèsia i analgèsia d’alta qualitat.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-2">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-2.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-2.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Seguretat</a></h3>
                                    <h5>Ens preocupem per a que tu no et preocupis</h5>
                                    <p>Treballem amb les condicions humanes i materials adequades. No assumeixis riscos: tracta amb professionals qualificats i experts.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-3">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-3.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-3.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Confort</a></h3>
                                    <h5>Treballem per aconseguir el màxim confort del pacient</h5>
                                    <p>Qualsevol procediment o intervenció comporta un estrès que condiciona una mala experiència i és totalment evitable. No dubtis en posar-te en les nostres mans.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-4">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-4.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-4.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Confiança</a></h3>
                                    <h5>Al teu costat des de l’inici fins al final del procés</h5>
                                    <p>Garantim una experiència positiva. Et sentiràs cuidat i acompanyat en tot moment. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-5">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-5.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-5.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Satisfacció</a></h3>
                                    <h5>El nostre objectiu, la nostra raó de ser</h5>
                                    <p>Els millors resultats d’un servei professional i humà amb la màxima seguretat clínica. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department-tab-6">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-6.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-6.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->

    <!--Fun Facts Section-->
    <section class="fun-fact-section" style="background-image:url([base_url]theme/theme/images/background/3-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-multiple-users-silhouette"></span>
                        <span class="count-text" data-speed="3000" data-stop="2350">0</span>
                        <h4 class="counter-title">Clients satisfets</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-people-1"></span>
                        <span class="count-text" data-speed="3000" data-stop="25">0</span>+
                        <h4 class="counter-title">Professionals</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-success"></span>
                        <span class="count-text" data-speed="3000" data-stop="12000">0</span>+
                        <h4 class="counter-title">Operacions</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                    <div class="count-box">
                        <span class="icon flaticon-surgeon-doctor"></span>
                        <span class="count-text" data-speed="3000" data-stop="80">0</span>+
                        <h4 class="counter-title">Clíniques satisfetes</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Facts Section-->















    <!-- Doctors Section -->
    <section class="department-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h2>La <span>Clínica</span></h2>
                <div class="separator"><span class="fa fa-plus"></span></div>
            </div> 

             <!-- Department Tab -->
            <div class="department-tabs tabs-box">
                <ul class="tab-buttons clearfix">
                    
                    <li data-tab="#department2-tab-2" class="tab-btn active-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-interview"></span>
                            <h5>Eficàcia</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-3" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-medal"></span>
                            <h5>Qualitat</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-4" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-justice"></span>
                            <h5>Seguretat</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-5" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-ecg"></span>
                            <h5>Externalització</h5>
                        </div>
                    </li>
                    <li data-tab="#department2-tab-1" class="tab-btn tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-friendship"></span>
                            <h5>Fidelització</h5>
                        </div>
                    </li>
                   <!-- 
 <li data-tab="#department2-tab-6" class="tab-btn">
                        <div class="icon-box">
                            <span class="icon flaticon-scientist"></span>
                            <h5>Laboratory</h5>
                        </div>
                    </li>
 -->
                </ul>

                <!--Tabs Container-->
                <div class="tabs-content">
                    <!--Tab / Active Tab-->
                    <div class="department-info tab" id="department2-tab-1">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-7.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-7.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Fidelització</a></h3>
                                    <h5>Un client satisfet és la millor publicitat</h5>
                                    <p>El millor servei i els millors resultats garanteixen una bona propaganda. Que parlin bé de tu!</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab active-tab" id="department2-tab-2">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-8.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-8.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Eficàcia</a></h3>
                                    <h5>Procediment eficaç i sense interferències</h5>
                                    <p>Cada procediment és únic i requereix la màxima concentració. Treballa amb bones condicions per aconseguir els millors resultats. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-3">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-9.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-9.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Qualitat</a></h3>
                                    <h5>En equip, oferim la millor assistència</h5>
                                    <p>Assegura un servei d’alta qualitat. Guanya competitivitat, posicionament i atracció del client. </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-4">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-10.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-10.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Seguretat</a></h3>
                                    <h5>És una qüestió de responsabilitat</h5>
                                    <p>No posis en risc el teu pacient i protegeix el teu negoci (col·legiació, responsabilitat civil, registre, documentació legal).</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-5">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-11.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-11.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Externalització i integració de serveis professionals</a></h3>
                                    <h5>Deixa-ho tot a les nostres mans</h5>
                                    <p>Despreocupa’t de la compra d’equips, monitors i fàrmacs. Oferim assessorament i suport logístic segons requeriments.</p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Tab -->
                    <div class="department-info tab" id="department2-tab-6">
                        <div class="row clearfix">
                            <!-- Image-column -->
                            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <div class="image-box">
                                        <a href="[base_url]theme/theme/images/resource/department-4-12.jpg" class="lightbox-image" data-fancybox="Gallery">
                                            <img src="[base_url]theme/theme/images/resource/department-4-12.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Content Column -->
                            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                                <div class="inner-column">
                                    <h3><a href="doctors.html">Gynecological Conditions</a></h3>
                                    <h5>Seeing a Gynecologist is part of Teenage Health</h5>
                                    <p>From puberty through menopause, a woman's reproductive organs are constantly changing through the normal processes of sexual activity, pregnancy and aging, and sometimes disease and injury. UCSF Medical Center is recognized as one of the top hospitals in the nation for gynecological </p>
                                    <div class="call-btn">
                                        <a href="[base_url]serveis.html" class="theme-btn btn-style-two">Serveis</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->
    <!-- Call To Action -->
    <section class="call-to-action black" style="margin-top:50px; background-image: url([base_url]theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Vols treballar amb nosaltres?</h2>
                    <p>
                        Omple el següent formulari i de seguida ens posarem en contacte, estarem encantats! 
                    </p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>

[footer]