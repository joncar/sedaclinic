<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');  
            $crud->set_clone();
            $crud->set_field_upload('theme','boletines')
                 ->set_field_upload('destinos','boletines')
                 ->field_type('emails_extras','tags')
                 ->add_action('Enviar','',base_url('notificaciones/admin/sendMail').'/');
            $this->loadView($crud->render());
        }

        function sendMail($id){
            $notificaciones = $this->db->get_where('notificaciones',array('id'=>$id));
            if($notificaciones->num_rows()>0){
                $notificaciones = $notificaciones->row();
                $folder = explode('-',$notificaciones->theme);
                $folder = 'boletines/'.$folder[0];
                $path = 'boletines/'.$notificaciones->theme;
                $excel = 'boletines/'.$notificaciones->destinos;
                $recent = false;
                $content = "";
                if(!is_dir($folder)){ //Si no se ha descomprimido aun lo hacemos
                    mkdir($folder);
                    //Descomprimir
                    $ext = explode('.',$notificaciones->theme);
                    if($ext[count($ext)-1]=='zip'){
                        $zip = new ZipArchive;
                        if ($zip->open($path) === TRUE)
                        {
                            $zip->extractTo($folder);
                            $zip->close();
                            $recent = true;
                        }
                    }                    
                }
                //Validamos si esta bien comprimido
                if(!file_exists($folder.'/content.html')){
                    echo 'Formato zip no válido - No se encuentra el fichero content.html';
                    erasedir($folder);
                    exit;
                }
                if($recent){ //Si fue recien creado se cambian las url
                    $content = file_get_contents($folder.'/content.html');
                    $content = str_replace('src="','src="'.base_url().$folder.'/',$content);
                    $content = str_replace('{link}',base_url().$folder.'/content.html',$content);
                    $content = str_replace('{baja}',base_url('paginas/frontend/unsubscribe/{email}'),$content);
                    file_put_contents($folder.'/content.html',$content);
                }
                //echo 'Fichero encontrado <hr>';
                if(empty($content)){
                    $content = file_get_contents($folder.'/content.html');
                }
                //echo $content;
                echo '<hr>Enviando correos<hr>';
                if(file_exists($excel)){
                    require_once(APPPATH.'libraries/Excel/SpreadsheetReader.php');                
                    $reader = new SpreadsheetReader($excel);
                    foreach($reader as $row){
                        if(preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i",$row[0])){
                            echo $row[0].'<br/>';
                            $email = str_replace('{email}',base64_encode($row[0]),$content);
                            correo($row[0],$notificaciones->titulo,$email);
                        }
                    }
                }
                if(!empty($notificaciones->emails_extras)){
                    foreach(explode(',',$notificaciones->emails_extras) as $row){
                        $row = trim($row);
                        echo $row.'<br/>';
                        $email = str_replace('{email}',base64_encode($row),$content);
                        correo($row,$notificaciones->titulo,$email);
                    }
                }
            }
        }

        function syncronize(){
            $this->db->query('truncate emails');
            $mbox = imap_open("{mallorcaislandfestival.com:143/novalidate-cert}", "reservas@mallorcaislandfestival.com", "i84%Ba5i", NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'))
                  or die("no se puede conectar: " . imap_last_error());
            $sorted_mbox = imap_sort($mbox, SORTDATE, 0); 
            $totalrows = imap_num_msg($mbox);
            
            for($i=0;$i<$totalrows;$i++){
                $data = array();
                $header = imap_fetchheader($mbox, $sorted_mbox[$i]);

                $subject = array();
                preg_match_all('/^Subject: (.*)/m', $header, $subject);
                $data['subject'] = utf8_encode($subject[1][0]);
                
                $subject = array();
                preg_match_all('/^Date: (.*)/m', $header, $subject);
                $data['fecha'] = strtotime($subject[1][0]);
                $data['fecha'] = date("Y-m-d H:i:s",$data['fecha']);

                $subject = array();
                preg_match_all('/^From: (.*)/m', $header, $subject);
                $data['email'] = $subject[1][0];

                $data['message'] = utf8_encode(imap_qprint(imap_body($mbox,$sorted_mbox[$i])));
                $this->db->insert('emails',$data);
            }

            imap_close($mbox); 
            echo 'Se han incorporado:'.$totalrows.' Emails a la bd <a href="'.base_url('notificaciones/admin/emails/success').'">Volver a los emails</a>';
        }

        function subscritos($x = ''){            
            $crud = $this->crud_function('','');              
            $crud = $crud->render();            
            $this->loadView($crud);         
        }
    }
?>
