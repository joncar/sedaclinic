$(document).on('ready',function(){
	$( "tbody" ).sortable({
	  revert: true,
	  stop: function( event, ui ) {ordenar(ui);}  
	});
	$( "tbody tr" ).draggable({
	  connectToSortable: "tbody",
	  helper: "clone",
	  revert: "invalid",      
	});	
	$( "ul, li" ).disableSelection();
	$( "tbody" ).on( "drop", function( event, ui ) {} );
});

function ordenar(el){
	var tr = $(el.item[0]).parents('tbody').find('.orderRow');
	var x = 0;
	var fields = [];
	var url = tr.parents('form').data('orderurl');
	tr.each(function(){
		fields.push($(this).data('id'));
		x++;
		if(x==tr.length){
			$.post(url,{fields:fields,page:crud_pagin},function(data){
				console.log(data);
			});
		}
	});
}